#!/bin/bash

#NARGS=1
#
#if [ ! $# -eq $NARGS ]
#then
#	echo "Usage of $(echo $0 | cut -d / -f2-):<pathcsvfile>"
#	exit 1
#fi

echo "Enter full path to csvfile (with header) containing the dataset representing the system to study"
read pathds

if [[ $pathds != /* ]]
then
	echo "Please enter full path for csvfile"
	exit 1
fi

headerp=$(head -1 $pathds | egrep -c "[a-z]")

if [ $headerp -eq 0 ]
then
	echo "The dataset must contain column names (header)"
	exit 1
fi

pm=$(ls | grep -c "matlabsources.dir")
pd=$(ls | grep -c "estimate_direct_effect.dir")
pe=$(ls | grep -c "buildcausalmodel.dir")
pi=$(ls | grep -c "distributed_independences.dir")
pr=$(ls | grep -c "Rsources.dir")

P=$(( $pm*$pd*$pe*$pi*$pr ))

if [ $P -eq 0 ]
then
	echo "This script should be run from the directory where the soureces are (matlabsources.dir estimate_direct_effect.dir buildcausalmodel.dir distributed_independences.dir Rsources.dir)"
	exit 1
fi

pathm=`pwd`/matlabsources.dir
pathr=`pwd`/Rsources.dir
verbose=1
#This script is to be run from the directory where all the sources are present in subdirs
choice1=0

while [ ! $choice1 -eq 5 ]
do
	echo "Choose option"
	echo "[1] Run distributed HSIC"
	echo "[2] Causal model inference"
	echo "[3] Direct effect computation"
	echo "[4] Prediction of intervention"
	echo "[5] Exit"
	read choice1

	if [ $choice1 -eq 1 ]
	then
		echo "Enter the path to file containing list of machines (workers)"
		read listmachines
		while [[ $listmachines != /* ]]
		do
			echo -e "Please enter full path:\t"
			read listmachines
		done
		if [ $verbose -gt 1 ]
		then
			echo "List machine is:"
			cat $listmachines
			read input
		fi
#		echo "Enter complete path to matlab resources directory"
#		read pathm
#		while [[ $pathm != /* ]]
#		do
#			echo -e "Please enter full path:\t"
#			read pathm
#		done
#		if [ $verbose -gt 1 ]
#		then
#			echo "Path to matlab resources is: $pathm"
#			ls $pathm
#			read input
#		fi
		echo "Enter the local directory to store results: "
		read resdir
		while [[ $resdir != /* ]]
		do
			echo "Enter full path: "
			read resdir
		done
		while [ ! -d $resdir ]
		do
			echo "Directory does not exist, create [Y/N]: "
			read c
			if [ "$c" == "Y" ]
			then
				mkdir $resdir
			else
				echo "Enter new dir"
				read resdir
				while [[ $resdir != /* ]]
		                do
                		        echo "Enter full path: "
		                        read resdir
                		done
			fi
		done
		echo "Enter directory in machines where resources will be stored: "
		read worker_rsrc_dir
		echo "Enter directory in machines where results will be stored: "
		read worker_rslt_dir
		echo "Enter the number of loops in bootstrap HSIC"
		read nloops
		if [ $verbose -gt 1 ]
		then
			echo "Number of loops is: $nloops"
			read input
		fi
		echo "Enter the subdataset size in bootstrap HSIC"
		read subdssize
		if [ $verbose -gt 1 ]
		then
			echo "Sub dataset size is: $subdssize"
			read input
		fi
		echo "Enter maximum conditioning set size"
		read maxC
		if [ $verbose -gt 1 ]
		then
			echo "Maximum conditioning set size is: $maxC"
			read input
		fi
		echo "Enter significance level to be used in independence test (default 0.05)"
		read alpha
		if [ $verbose -gt 1 ]
		then
			echo "Significance value is: $alpha"
			read input
		fi
		if [ $verbose -gt 1 ]
		then
			echo "About to launch centralnode script from dir distributed_independence.dir (it gonna take some time (possibly days)"
		fi
		#
		#<listworkers><pathdataset><matlabresourcespath><localresourcesdir><worker_resources_dir><worker_results_dir><maxcondsetsize><bootstrap_subdssize><bootstrap_loop><hsic_significancelevel><resdir>
		idx=0
		lresources=$resdir/intermediary_results_dist_hsic.dir
		while [ -d $lresources ]
		do
	                lresources=$resdir/intermediary_results_dist_hsic_$idx.dir
			idx=$(( $idx+1 ))

		done
		mkdir $lresources
		echo "(cd distributed_independences.dir && ./centralnode.sh $listmachines $pathds $pathm $lresources $worker_rsrc_dir $worker_rslt_dir $maxC $subdssize $nloops $alpha $resdir)"
		(cd distributed_independences.dir && ./centralnode.sh $listmachines $pathds $pathm $lresources $worker_rsrc_dir $worker_rslt_dir $maxC $subdssize $nloops $alpha $resdir)

	elif [ $choice1 -eq 2 ]
	then
		echo "Which algorithm"
		echo "[1]: PC"
		echo "[2]: IC"
		read choice2
		if [ $choice2 -eq 1 ]
		then
			algo=PC
		elif [ $choice2 -eq 2 ]
		then
			algo=IC
		else
			echo "Wrong choice"
			exit 1
		fi
		if [ $verbose -gt 1 ]
		then
			echo "You chose $algo algorithm"
			read input
		fi

		echo "Which independence criterion"
		echo "[1]: Z-Fisher (normally distributed, linear dependences)"
		echo "[2]: HSIC (no constraints on distribution and dependences but computational time tends to grow exponentially with sample size)"
		echo "[3]: Oracle (Results have then to be stored in a csvfile in the form [X,Y,Z1,..Zn,Result], the one returned by the distributed HSIC f.ex)"
		read choice3
		if [ $choice3 -eq 1 ]
		then
			indepc=ZFisher
			pathoracle=''
		elif [ $choice3 -eq 2 ]
		then
			indepc=HSIC
			pathoracle=''
		elif [ $choice3 -eq 3 ]
		then
			indepc=Oracle
			echo -e "Please enter the path to csvfile containing oracle results:\t"
			read pathoracle
			while [[ $pathoracle != /* ]]
			do
				echo -e "Please enter full path:\t"
				read pathoracle
			done
		else
			echo "Wrong choice"
			exit 1
		fi
		if [ $verbose -gt 1 ]
		then
			echo "You chose $indepc independence criterion"
			read input
		fi

		echo -e "Enter the threshold (significance level for Fisher/HSIC, percentage for distributed HSIC oracle file):\t"
		read threshold
		if [[ -z $resdir ]]
		then
			echo -e "Enter the directory to store the result:\t"
			read pathout
			while [[ $pathout != /* ]]
			do
				echo -e "Please enter full path:\t"
				read pathout
			done
			while [ ! -d $pathout ]
			do
				echo "This directory does not exist"
				echo "Create it [Y/N]:"
				read crdir
				if [ "$crdir" == "Y" ]
				then
					mkdir $pathout
				else
					echo -e "Enter existing directory:\t"
					read pathout
					while [[ $pathout != /* ]]
					do
						echo -e "Please enter full path:\t"
						read pathout
					done
				fi
			done
		else
			pathout=$resdir
			while [ ! -d $pathout ]
                        do
				echo "The result dir $pathout"
                                echo "This directory does not exist"
                                echo "Create it [Y/N]:"
                                read crdir
                                if [ "$crdir" == "Y" ]
                                then
                                        mkdir $pathout
                                else
                                        echo -e "Enter existing directory:\t"
                                        read pathout
                                        while [[ $pathout != /* ]]
                                        do
                                                echo -e "Please enter full path:\t"
                                                read pathout
                                        done
                                fi
                        done
		fi
#		echo "Enter complete path to matlab resources directory"
#		read pathm
#		while [[ $pathm != /* ]]
#		do
#			echo -e "Please enter full path:\t"
#			read pathm
#		done
		fileout=$(matlab -nodesktop -nosplash -nojvm -r "addpath('$pathm');set_path_2('$pathm');learn_struct_wrapper('$algo','$pathds','$pathout','$indepc','$pathoracle',$threshold);exit" | grep "#causal model#" | cut -d : -f2-)
		echo "The result was saved in $fileout"
		cp -r buildcausalmodel.dir/reformat_graph/ $pathout
		echo "(cd $pathout/reformat_graph && ./main_script_generic $fileout $pathds $choice2 $pathout)"
		(cd $pathout/reformat_graph && ./main_script_generic $fileout $pathds $choice2 $pathout)
		display $pathout/jpgfiles.dir/*.jpg &
		patterndot=$(echo $fileout | tr / "\n" | tail -1 | tr _ % | sed -re 's/\.csv//g')
		dotfile_p=$(find $pathout -iname "*.dot" | grep "$patterndot" | head -1)
		modelc=$fileout

	elif [ $choice1 -eq 3 ]
	then
		echo -e "Method_id:\n[1] Kernel Regression\n[2] Log linear regression"
		read methodid
		while [ ! $methodid -eq 1 ] && [ ! $methodid -eq 2 ]
		do
			echo "Wrong choice, 1 or 2: "
			read methodid
		done	
		#estimate_direct_effect.dir/main_script_direct_effect <pathmodel><pathdataset><path_dot_file><method_id><pathout>
		if [[ -z $modelc ]] 
		then
			echo "Enter the path to the matrix containing the causal graph model: "
			read modelc

			while [[ $modelc != /* ]]
			do
				echo "Enter full path: "
				read modelc
			done
		fi
		if [[ -z $pathds ]]
		then
			echo "Enter the path to the file containing the dataset: "
                        read pathds

                        while [[ $pathds != /* ]]
                        do
                                echo "Enter full path: "
                                read pathds
                        done
		fi
		if [[ -z $dotfile_p ]]
		then
			echo "Enter the path to the dot file containing the model of the causal graph: "
			read dotfile_p
			while [[ $dotfile_p != /* ]]
                        do
                                echo "Enter full path: "
                                read dotfile_p
                        done
		fi

		if [[ -z $pathout ]]
		then
			echo "Enter path to directory to store results: "
			read pathout
			while [[ $pathout != /* ]]
                        do
                        	echo -e "Please enter full path:\t"
                                read pathout
                        done
			while [ ! -d $pathout ]
			do
				echo "This directory does not exist"
				echo "Create it [Y/N]:"
				read crdir
				if [ "$crdir" == "Y" ]
				then
					mkdir $pathout
				else
					echo -e "Enter existing directory:\t"
					read pathout
					while [[ $pathout != /* ]]
					do
						echo -e "Please enter full path:\t"
						read pathout
					done
				fi
			done
			
		fi
		if [ $methodid -eq 1 ]
		then
			pathout2=$pathout/kernel_regression.dir
			while [ -d $pathout2 ]
			do
				echo "$pathout2 already exists, Remove:[Y/N] "
				read ch
				if [ "$ch" == "Y" ]
				then
					rm -rf $pathout2
				else
					echo "Enter $pathout subdir to store results: "
					read subd
					pathout2=$pathout/$subd
				fi
			done
			mkdir $pathout2
				
			pr=$(cd $pathr && ./script_check_package np | grep -ic "installed")
			if [ $pr -eq 0 ]
			then
				echo "np library not installed, will be installed manually"
				echo "Enter directory to store R libs"
				read Rlibdir
				while [[ $Rlibdir != /* ]]
				do
					echo "Enter full path: "
					read Rlibdir
				done
				if [ ! -d $Rlibdir ]
				then
					mkdir -p $Rlibdir
				fi
				pnp=$(find $Rlibdir -iname "np" | grep -c "R/np")
				if [ $pnp -eq 1 ]
				then
					echo "np package seems to be present. Install anyway [Y/N]: "
					read input
					if [ "$input" == "Y" ]
					then
						(cd $pathr && ./installRsources $pathr $Rlibdir)
					fi
				else
					(cd $pathr && ./installRsources $pathr $Rlibdir)
				fi
				if [ $verbose -gt 0 ]
				then
					echo "Start direct effects estimation"
					echo "(cd estimate_direct_effect.dir && ./main_script_direct_effect $modelc $pathds $dotfile_p $methodid $pathout2 $Rlibdir)"
				fi
				(cd estimate_direct_effect.dir && ./main_script_direct_effect $modelc $pathds $dotfile_p $methodid $pathout2 $Rlibdir)
			else
				(cd estimate_direct_effect.dir && ./main_script_direct_effect $modelc $pathds $dotfile_p $methodid $pathout)
			fi
		else
			pathout2=$pathout/lognormal.dir
			while [ -d $pathout2 ]
			do
				echo "$pathout2 already exists, Remove:[Y/N] "
				read ch
				if [ "$ch" == "Y" ]
				then
					rm -rf $pathout2
				else
					echo "Enter $pathout subdir to store results: "
					read subd
					pathout2=$pathout/$subd
				fi
			done
			mkdir $pathout2

			(cd estimate_direct_effect.dir && ./main_script_direct_effect $modelc $pathds $dotfile_p $methodid $pathout)
		fi
	elif [ $choice1 -eq 4 ]
	then
		echo "Start the prediction of Pr(Y=y | do(X=x))"
		echo "Use:"
		echo "[1] Back door criterion"
		echo "[2] Front door criterion"
		echo "Your choice: "
		read choice4
		if [ $choice4 -eq 1 ]
		then
			echo "Back door adjustement is: Pr(Y | do(X=x)) = Sum_{Z} Pr(Y=y | X=x, Z=z)Pr(Z=z)"
			echo -e "With Z blocking set:\n- No node in Z descendant of X\n- Z blocks every path between X and Y going into X"
		elif [ $choice -eq 2 ]
		then
			echo "Front door adjustement is: Pr(Y | do(X=x)) = Sum_{Z} Pr(Z=z | X = x)*Sum_{X'} Pr(Y=y | Z=z, X'=x')*Pr(X'=x')"
			echo -e "With Z blocking set:\n- Z intercepts every path between X and Y\n- There is no back-door path from X to Z\n- Every back-door path from Z to Y are blocked by X"
		else
			echo "Wrong choice"
			exit 1
		fi
		
		echo "List of variables"
		head -1 $pathds | tr , "\n" | nl | sed -re 's/^[ ]*/[/g' | sed -re 's/([0-9]+)[ \t]*/\1] /g'
		echo "Enter Y number: "
		read Y
		echo "Enter X number: "
		read X
		echo "Enter Z number(s) separated by commas"
		read z
		#main_bdcopula2d(ds,dimy,dimx,valx,deltax,dimz,N,methodk,methodc)
		echo "Enter the value of intervention"
		read valx
		echo "Enter delta x"
		read deltax
		echo "Enter the number of points for estimating marginals"
		read Nmargin
		echo "Choose one kernel method for marginal estimation [default normal]"
		echo "[1] Normal"
		echo "[2] Box"
		echo "[3] Triangle"
		echo "[4] Epanechnikov"
		echo "Your choice: "
		read kerneln
		if [ $kerneln -eq 1 ]
		then
			kernelm=normal
		elif [ $kerneln -eq 2 ]
		then
			kernelm=box
		elif [ $kerneln -eq 3 ]
		then
			kernelm=triangle
		elif [ $kerneln -eq 4 ]
		then
			kernelm=epanechnikov
		else
			echo "Wrong choice"
			exit 1
		fi
		if [ $verbose -gt 1 ]
		then
			echo "The chosen method is $kernelm"
		fi

		echo "Chooce one copula method for estimating the multi variate pdf [default t]"
		echo "[1] Gaussian"
		echo "[2] t"
		echo "Your choice"
		read copulanb
		if [ $copulanb -eq 1 ]
		then
			copulam=Gaussian
		elif [ $copulanb -eq 2 ]
		then
			copulam=t
		else
			echo "Wrong choice"
			exit 1
		fi
		if [ $verbose -gt 1 ]
		then
			echo "The chosen method is $copulam"
		fi
		
		#Reformat Z to [z1,z2,...]
		Z="["
		nz=$(echo $z | tr , "\n" | wc -l | cut -d \  -f1)
		if [ $verbose -gt 1 ]
		then
			echo "The blocking set size is $nz"
		fi

		if [ $nz -gt 2 ]
		then
			echo "For resource constraints, no implementation of backdoor/frontdoor criterion for blocking set size bigger than 2"
			exit 1
		fi

		for (( i=1;i<=$nz;i++ ))
		do
			Z=$(echo "$Z$(echo $z | cut -d , -f$i),")
		done
		Z=$(echo $Z | cut -d , -f1-$nz)
		Z=$(echo "$Z]")
		if [ $verbose -gt 1 ]
		then
			echo "Z set is $Z"
		fi

		#Redefine matalb function, read ds, write output, choose pathout		
		echo -e "Enter the directory to store the result:\t"
		read pathout
		while [[ $pathout != /* ]]
		do
			echo -e "Please enter full path:\t"
			read pathout
		done
		while [ ! -d $pathout ]
		do
			echo "This directory does not exist"
			echo "Create it [Y/N]:"
			read crdir
			if [ "$crdir" == "Y" ]
			then
				mkdir $pathout
			else
				echo -e "Enter existing directory:\t"
				read pathout
				while [[ $pathout != /* ]]
				do
					echo -e "Please enter full path:\t"
					read pathout
				done
			fi
		done

		if [ $choice4 -eq 1 ]
		then
			if [ $nz -eq 0 ]
			then
				matlab -nodesktop -nosplash -nojvm -r "addpath('$pathm');set_path2('$pathm');main_bd_copula0d_wrapping('$pathds',$Y,$X,$valx,$deltax,$Nmargin,'$kernelm','$copulam','$pathout');"
			elif [ $nz -eq 1 ]
			then
				matlab -nodesktop -nosplash -nojvm -r "addpath('$pathm');set_path_2('$pathm');main_bdcopula1d_wrapping('$pathds',$Y,$X,$valx,$deltax,$Z,$Nmargin,'$kernelm','$copulam','$pathout');"
			elif [ $nz -eq 2 ]
			then
				#function [x,fx,ex,sx,mx,ex2,sx2] = main_bdcopula2d(ds,dimx,dimy,valy,deltay,dimz,N,methodk,methodc)
				#[x,fx,ex,sx,mx,ex2,sx2] = main_bdcopula2d_wrapping(pathds,dimx,dimy,valy,deltay,dimz,N,methodk,methodc,pathout)
				matlab -nodesktop -nosplash -nojvm -r "addpath('$pathm');set_path_2('$pathm');main_bdcopula2d_wrapping('$pathds',$Y,$X,$valx,$deltax,$Z,$Nmargin,'$kernelm','$copulam','$pathout');"
			else
				echo "No implementation for conditioning set bigger than 2"
				exit 1
			fi
		elif [ $choice4 -eq 2 ]
		then
			echo "Front door adjustement not implemented yet"
			exit 1
		fi
	elif [ $choice1 -eq 5 ]
	then
		echo "Exiting program"
		echo "Give your feedback to hours@eurecom.fr"
		exit 0
	else
		echo "Wrong choice"
		exit 1
	fi
done
