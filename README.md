Author: Hadrien Hours     
Last update: September 2005

#        DESCRIPTION AND REQUIREMENTS FOR THE CAUSAL PACKAGE           

## Description

This package provides some code and procedures to lead a causal study

The main components are:
1) distributed Hilbert Schmidt Independence Criterion   
2) causal model inference   
3) direct effect estimation   
4) prediction of an intervention      

We now describe the different components: 

1) This components consists in combining the HSIC (from K. Zhang, J. Peters, D. Janzing and B. Scholkopf, Kernel-based Conditional Independence Test and Application in Causal Discovery) with a bootstrap method and distributing the different test to a list of workers (remote machines) and store the output at a centralized node:
    * Methods:     
        + HSIC + boostrap, consisting in testing l times the independence between X and Y on subdataset of size N, generated from random resampling of the original dataset    
    * Limitations:  
        + You have to define the maximum conditioning set size

2) Causal model inference:
    * Methods proposed    
        + PC     
	    + IC     
    * Limitations: Only three independence test:     
        + ZFisher,   
        + HSIC,    
        + Oracle from distributed HSIC    

3) Direct effect estimation:
    * Methods proposed:    
        + Single door criterion   
    * Limitations:    
    	+ Manual choice of the blocking set     
	Only two choices of regressions models: loglinear (Matlab), kernel (R)   

4) Predicting intervention:
    * Methods proposed:
	    + Back door criterion    
	* Limitations:
	    + Manual choice of the blocking set    
	    + Only valid for blocking set of size <= 2    

## Requirements

1) Several machines running matlab, the number of independences to test depends on the number of parameters and their dependences. Apart from the unconditional test, 1 conditional test for bootstrap+hsic with default pars (see below) takes up to 1 hour (this can be improved by decreasing the l and N) so the more machines the better
    * A central machine, coordinating the distribution and succession of task:
	    + Linux OS   
		+ Matlab   
		+ 50Mo at least    
		+ ssh access to remote machines (workers)     
	* A remote machine on which intermediary results will be stored, as well as sources (matlab/bash). IT IS ASSUMED THAT THIS MACHINE IS AN SHARED FOLDER WITH WORKERS !    
	* Workers that will run each independence test:
		+ Matlab (at least 2012 versions or performance are highly impacted)    
		+ 30Mo of space    
		+ All of them have direct access to the Master

2) One machine runing matlab (should be the central machine of 1)a)) with 50Mo of space to store the code, linux OS. Matlab statistical toolbox

3) Same as 2) + R with library np if kernel regression should be used (still working on integrating it in the package)

4) Same as 2)


#            RUNING AND PARAMETERS FOR THE CAUSAL PACKAGE              

The main script is called causal_study and takes as argument (complete) path to csvfile containing the dataset WITH HEADER
	It should be run from its package with corresponding sources present: buildcausalmodel.dir, distributed_independences.dir, matlabsources.dir

You are proposed 4 choices that corresponds to the chronological way of our leading of a causal study and they are described hereafter

1) Run the distributed hsic.
    * You will be asked to provide: 
	    + a remote machine on which intermediary results will be stored (the script check its existence with ping)    
		+ a remote directory on which intermediary results will be stored (created if does not exist)    
	    + a file with a list of machine to run matlab tasks on   
		+ The parameters for the hsic are:    
		    - number of loops: default 80    
			- subsize: default 400    
			- maxcondset: 3 if number of variables < 15 (empirical)    
			- siginificance level: 0.05 (usual signifiance level)    
			

2) TODO

3) TODO

4) TODO






Acknowledgements and other source

Matlab bnt package
HSIC implementation from K. Zhang, J. Peters, D. Janzing and B. Scholkopf, Kernel-based Conditional Independence Test and Application in Causal Discovery
R library np
