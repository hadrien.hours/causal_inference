args = commandArgs(trailingOnly=TRUE)
len <- length(args)

#for (i in c(1:len)){
#print(sprintf('%d',i))
#print(args[i])
#} 
#print(commandArgs(trailingOnly=TRUE))

verbose=2

dspath<-args[1]
dirout<-args[len-1]
rpath<-args[len]
ydim <- as.numeric(args[2])
xdim <- as.numeric(args[3])

zdims <- NULL

if (len > 5){
 #len args takes into account args[0] script name
 for (i in seq(4,len-2)){
	zv <- as.numeric(args[i])
 	zdims <- c(zdims,zv)
	print(sprintf('%d',zv))
 }	
}

#Load data
ds <- read.csv(dspath,header=T)
#Load non parametric library, if manually installed np package then load library from previously installed libraries
if (rpath == ""){
	library(np)
}else
	if (verbose > 1){
		print(paste('The dir with R sources is ',rpath,sep=""))
	}
	library(cubature,lib.loc = rpath)
	library(np,lib.loc = rpath)

#Create regression formula of y on x and z
f1 <- paste('npregbw(ds[,',as.name(ydim),'] ~ ds[,',as.name(xdim),']')
if (len > 5){
	for (i in c(1,length(zdims))){
		f1 <-paste(f1,' + ds[,',as.name(zdims[i]),']')
	}
}
f1 <- paste(f1,', data=ds)')

#Perform the regression
bw <-eval(parse(text=f1))

#Get the bandwidth and scale factor
bwx <- bw$bw[1]
sfx <- bw$sfactor$x[1]
mydata <- paste(as.name(xdim),',',as.name(ydim),',',as.name(sfx),',',as.name(bwx),sep="")

#Write data in a csvfile
fout <- paste(dirout,'/direct_effect_',as.name(xdim),'_on_',as.name(ydim),'_blocking_set',sep="")

if (len>5){
	for (i in c(1,length(zdims))){
		fout <- paste(fout,'_',as.name(zdims[i]),sep="")
	}
}else
	 fout <- paste(fout,'_',as.name(0),sep="")

fout <- paste(fout,'.csv',sep="")
if (verbose > 1){
	print(paste('File will be saved as ',fout,sep=""))
}

#Add a header
header <- 'x,y,scale_factor,bandwidth'
datafile <- file(fout,open='wt')
writeLines(header,datafile)
writeLines(mydata,datafile)
close(datafile)
