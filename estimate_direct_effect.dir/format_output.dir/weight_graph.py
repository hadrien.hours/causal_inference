#!/bin/python

import sys
import csv

filedot = sys.argv[1]
filew = sys.argv[2]
listn = sys.argv[3]
fileout = sys.argv[4]

map={}
wdir = {}

#Create list where map[i] contains the name of par number i
with open(listn, 'rb') as csvfile:
	incsv = csv.reader(csvfile,delimiter=',')
	for row in incsv:
		val1=int(row[0])
		val2=str(row[1])
		map[val1] = val2
	
print map
#Create a dir where wdir[par1,par2] = scale_par1_par2,bw_par1_par2
with open(filew, 'rb') as csvfile:
        has_header = csv.Sniffer().has_header(csvfile.read(1024))
        csvfile.seek(0)
        incsv = csv.reader(csvfile,delimiter=',')
        if has_header:
                next(incsv)  # skip header row
	#endif
        for row in incsv:
                val1=int(row[0])
                val2=int(row[1])
		scale=float(row[2])
		bw = float(row[3])
		#Verify existence TODO
                wdir[(map[val1],map[val2])] = (scale,bw)
	
	#endfor
#endwith

print wdir

fid=open(fileout,'wb')
fid2=open(filedot,'rb')

for line in fid2:
	if line.find('->') == -1:
		fid.write(line)
	else:
		nodes=line.split('->')
		print nodes[0].strip()+','+nodes[1].strip()
		try:
			weight = wdir[(nodes[0].strip(),nodes[1].strip())]
			print nodes[0]+' -> '+ nodes[1] + ' [label = '+str(weight[0])+','+str(weight[1])+']'
			fid.write(nodes[0].strip()+' -> '+ nodes[1].strip() + ' [label = "'+str(weight[0])+','+str(weight[1])+'"]\n')
		except :
			print 'exception with '+line
			fid.write(line)
	#endif
#endfor
fid.close()
fid2.close()
