#!/bin/python

import sys
import csv

verbose=1

if (len(sys.argv) != 3):
	print sys.argv[0]+' needs 2 args (pathmodel,pathout)'
	sys.exit(1)
#endif

pathmodel=sys.argv[1]
pathout=sys.argv[2]

fid = open(pathmodel,'rb')
fid2=open(pathout,'wb')

fid2.write('x,y\n')
reader = csv.reader(fid,delimiter=',')

counter_r=0

counteffect=0
for row in reader:
	counter_r=counter_r+1
	counter_c=0
	for col in row:
		counter_c=counter_c+1
		if (int(col)==-1):
			fid2.write(str(counter_r)+','+str(counter_c)+'\n')
			counteffect=counteffect+1
		#endif
	#endfor
#endfor

if (verbose > 0):
	print str(counteffect)+' direct effect detected\n'
#endif

fid2.close()
fid.close()
