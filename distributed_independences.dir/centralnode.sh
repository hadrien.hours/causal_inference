#!/bin/bash
NARGS=11

if [ ! $# -eq $NARGS ]
then
	echo "Usage of $(echo $0 | cut -d / -f2):<listworkers><pathdataset><matlabresourcespath><localresourcesdir><worker_resources_dir><worker_results_dir><maxcondsetsize><bootstrap_subdssize><bootstrap_loop><hsic_significancelevel><resdir>"
	exit 1
fi

listworkers=$1
pathds=$2
pathm=$3
lresourcesfold=$4
wresults_dir=$6
wresources_dir=$5
maxconds=$7
N=$8
loops=$9
alpha=${10}
resdir=${11}
npars=$(head -1 $pathds | tr , "\n" | nl | tail -1 | awk '{print $1}')

testphase=0
verbose=1

if [ $verbose -gt 1 ]
then
	echo "pathds: $pathds"
	echo "pathm: $pathm"
	echo "pathresources: $lresourcesfold"
	echo "listworkers"
	cat $listworkers
	echo "Worker dir for resources: $wresources_dir"
	echo "Worker dir for results:  $wresults_dir"
	read input
fi


keeplog=1

if [ $keeplog -gt 0 ]
then
	logfilesdir=$resdir/logfiles.dir
	if [ ! -d $logfilesdir ]
	then
		mkdir $logfilesdir
	fi
fi

##Set resources on workers
#open a master ssh cnxs
echo "Creating ssh-agent"
eval `ssh-agent`
ssh-add
SSH_PROC_ID=$(ps kstart_time auxT | grep "ssh-agent" | grep -v "grep" | tail -1 | awk '{print $2}')

#Check reachability
listnodes=listnodes_checked

if [ -f $listnodes ]
then
	rm $listnodes
fi

for g in $(cat $listworkers)
do
        echo "Testing reachability of node $g"
        pres=$(ping -c 2 $g | egrep -o "[0-9]+% packet loss" | egrep -o "[0-9]+%" | egrep -o "[0-9]+")
        if [ $pres -lt 25 ]
        then
                echo $g >> $listnodes
        else
                echo "$g not reachable out of the list"
        fi
done
nnodes=$(wc -l $listnodes | cut -d \  -f1)


#Filenaming
nameds=$(echo $pathds | awk -F/ '{print $NF}' | sed -re 's/\.[a-z]*$//g' | tr . - )
wresultsfold=${wresults_dir%/}/$nameds\_$N\_$loops\_$(echo $alpha | tr . - )\_$maxconds\_results.dir
wresourcesfold=$wresources_dir
wmatlabfold=$wresources_dir/matlabsources.dir
lresultsfold=$resdir/$nameds\_results.dir
matlabdir=matlabsources.dir
#Create local dir for results and resources
while [ -d $lresultsfold ]
do
	echo "$lresultsfold already exists, delete: [Y/N] "
	read choice
	if [ "$choice" == "Y" ]
	then
		rm -r $lresultsfold
	else
		echo "Enter new directory for storing the results locally in $resdir"
		read lresultsfold_t
		if [ $(echo $lresultsfold_t | grep -c "$resdir") -eq 1 ]
		then
			lresultsfold=$lresultsfold_t
		else
			echo "$lresultsfold_t not subdir of $resdir"
		fi
	fi
done

if [ ! -d $lresourcesfold ]
then
	mkdir $lresourcesfold
fi

if [ ! -d $lresultsfold ]
then
	mkdir $lresultsfold
fi

lresultsfile=$lresultsfold/main_file_results_$nameds\_independences.csv

tar -cvf matlabsources.tar -C $pathm .


#create remote directories and copy resources					#### TO BE DONE ALLOW ONE DIFFERENT DIR FOR EACH REMOTE HOST AND STORE THE DIR NAMES IN A TEXT FILE, READ ONCE FINISHED TO DL RESULTS FROM WORKERS #####
indexnode=0
for l in $(cat $listnodes)
do
	indexnode=$(( $indexnode+1 ))
	if [ $verbose -gt 1 ]
	then
		echo "Index for node $l is $indexnode"
	fi

	nlpath=$(echo $wresultsfold | tr / "\n" | wc -l | cut -d \  -f1)
	if [ $nlpath -gt 1 ]
	then
		part1=$(echo $wresultsfold | cut -d / -f1-$(( $nlpath -1 )))
	fi
	part2=$(echo $wresultsfold | tr / "\n" | tail -1 | sed -re 's/\.dir//')_$indexnode.dir
	wresultsfold_node=$part1/$part2

	if [ $verbose -gt 1 ]
	then
		echo "For node $l the resultfolder is $wresultsfold_node"
		read input
	fi

	nlpath=$(echo $wresourcesfold | tr / "\n" | wc -l | cut -d \  -f1)
	if [ $nlpath -gt 1 ]
	then
		part1=$(echo $wresourcesfold | cut -d / -f1-$(( $nlpath -1 )))
	fi
	part2=$(echo $wresourcesfold | tr / "\n" | tail -1 | sed -re 's/\.dir//')_$indexnode.dir
	wresourcesfold_node=$part1/$part2
	
	if [ $verbose -gt 1 ]
	then
		echo "For node $l the resource folder is $wresourcesfold_node"
	fi
	#create directories
	wmatlabfold_node=$wresourcesfold_node/matlabsources.dir
	presdir=$(ssh $l "if [ -d $wresultsfold_node ]; then echo \"Dir already exists\"; fi" | grep -ic "already exists")
	psrcdir=$(ssh $l "if [ -d $wresourcesfold_node ]; then echo \"Dir already exists\"; fi" | grep -ic "already exists")

	if [ $presdir -gt 0 ]
	then
		echo "$wresultsfold_node already exists on node $l. For simplicity all workers store results in same (initially empty) directory. Delete items in this remote directory: [Y/N] "
		read delwres
		if [ "$delwres" == "Y" ]
		then
			ssh $l "rm -r $wresultsfold_node"
		else
			exit 1
		fi
	fi
	ssh $l "mkdir -p $wresultsfold_node"

	if [ $psrcdir -gt 0 ]
	then
		echo "$wresourcesfold_node already exists on node $l. For simplicity all workers store results in same (initially empty) directory. Delete items in this remote directory: [Y/N] "
		read delwres
		if [ "$delwres" == "Y" ]
		then
			ssh $l "rm -r $wresourcesfold_node"
		else
			exit 1
		fi
	fi
	ssh $l "mkdir -p $wresourcesfold_node"
	ssh $l "mkdir -p $wmatlabfold_node"

	#copy resources (matlab and dataset)
	scp matlabsources.tar $l:$wmatlabfold_node
	if [ $verbose -gt 0 ]
	then
		echo "Start decompressing source on node $l"
	fi
	ssh $l "(cd $wmatlabfold_node && tar -xf matlabsources.tar)"&
	process_tab[$indexnode]=$!
	if [ $verbose -gt 0 ]
	then
		echo "Process id ${process_tab[$indexnode]}"
	fi
	if [ $verbose -gt 1 ]
	then
		echo "Matlab resources copied to $l:$wmatlabfold_node and being untared (background)"
		read input
	fi
	scp $pathds $l:$wresourcesfold_node
done

rm -f matlabsources.tar

#Check untar of matlab resources are finished
pres=1
while [ $pres -gt 0 ]
do
	pres=0
	for (( l=1;l<=$indexnode; l++ ))
	do
		p=$(ps aux | grep "ssh" | egrep -c "[ \t]+${process_tab[$l]}[ \t]+")
		pres=$(( $pres+$p ))
	done
	echo "Waiting for untar of matlab resources to finish on remote nodes"
	echo "Sorry"
	sleep 40
done
#p=$(ps aux | grep ssh | grep "$l" | grep -c tar)
#
#while [ $p -gt 0 ]
#do
#	echo "Wait for untar finishing on node $l"
#	sleep 30
#	p=$(ps aux | grep ssh | grep "$l" | grep -c tar)
#done
#
#for l in $(cat $listnodes)
#do
#        indexnode=$(( $indexnode+1 ))
#
#        nlpath=$(echo $wresourcesfold | tr / "\n" | wc -l | cut -d \  -f1)
#        if [ $nlpath -gt 1 ]
#        then
#                part1=$(echo $wresourcesfold | cut -d / -f1-$(( $nlpath -1 )))
#        fi
#        part2=$(echo $wresourcesfold | tr / "\n" | tail -1 | sed -re 's/\.dir//')_$indexnode.dir
#        wresourcesfold_node=$part1/$part2
#        wmatlabfold_node=$wresourcesfold_node/matlabsources.dir
#	ssh $l "rm -f $wmatlabfold_node/matlabsources.tar"
#done

#create log dir for current work
if [ $keeplog -gt 0 ]
then
	logdir=$logfilesdir/$nameds\_logs.dir
	while [ -d $logdir ]
	do
		echo "$logdir already exists, enter new name: "
		read logdir
		while [[ $logdir != /* ]]
		do
			echo "Enter full path"
			read logdir
		done
	done
	mkdir $logdir
	mkdir $logdir/results.dir
	mkdir $logdir/independences.dir
fi

if [ $verbose -gt 1 ]
then
	echo "Remote directories (last node)"
	echo -e "\tResults dir: $wresultsfold_node"
	echo -e "\tResources dir: $wresourcesfold_node"
	echo -e "\tMatlab dir: $wmatlabfold_node\n\n"
	echo "Local directories"
	echo "Resources dir: $lresourcesfold"
	echo "Result dir: $lresultsfold"
	echo "Result file: $lresultsfile"
	read input
fi


#create resultfile
header="X,Y"
for (( i=1;i<=$maxconds;i++ ))
do
        header="$header,Z$i"
done
header="$header,N,l,alpha,percentage"
echo "$header" > $lresultsfile

##launch jobs
nameds=$(echo $pathds | awk -F/ '{print $NF}')
for (( i=0;i<=$maxconds;i++ ))
do
        if [ $i -eq 0 ]
        then
                listindeptested=''
        else
                listindeptested="$lresourcesfold/listindep_cond_${i}_$nameds\_tested"
                #Generate distinct x and y independent from testested independences
                matlab -nodesktop -nosplash -nojvm -r "selectTestedIndependences('$lresultsfile','$listindeptested');exit"
		if [ $verbose -gt 1 ]
		then
			echo "The list of already tested independences, before generating the ones left to test for cond set size $i, is stored in $listindeptested"
		fi
        fi
        listindep="$lresourcesfold/listindep_cond_${i}_$nameds"

        #Generate list of independences to test
        matlab -nodesktop -nosplash -nojvm -r "generateindependencelist($npars,$i,'$listindeptested','$listindep');exit"

	n2t=$(wc -l $listindep | cut -d \  -f1)

	#if file contains only one line, only header, no more independences to be tested
	if [ $n2t -gt 1 ]
	then
	        #Create dir to store results
		lresultsdircond=$lresultsfold/results_cond_$i.dir
		
		if [ -d $lresultsdircond ]
		then
			echo "$lresultsdircond exists, remove contained files? Y/N: "
			read choice
			if [ "$choice" == "Y" ]
			then
				rm -rf $lresultsdircond
			else
				echo "Enter new name for subdir fo $lresultsfold and condset $i"
				read subdirc
				lresultsdircond=$lresultsfold/$subdirc
			fi
		fi
		mkdir $lresultsdircond

		
		#Test independences
		if [ $verbose -gt 0 ]
		then
			echo "Start to distribute jobs for conditional sets of size $i"
			echo "Launching python distributeJobs.py $listindep $nnodes $listnodes $wresourcesfold $matlabdir $nameds $wresultsfold $N $loops $alpha"
			echo "The number of independence to be tested: $(wc -l $listindep | cut -d \  -f1)"
		fi
		python distributeJobs.py $listindep $nnodes $listnodes $wresourcesfold $matlabdir $nameds $wresultsfold $N $loops $alpha
		if [ $verbose -gt 0 ]
		then
			echo "Finished testing indepedences for conditional sets of size $i"
		fi

		#Download and reformat independences to add them to resultfile
		if [ $verbose -gt 1 ]
		then
			echo "Download results from the remote $nnodes workers"
		fi
		indenode=0
		for l in $(cat $listnodes)
		do
			indexnode=$(( $indexnode+1 ))
			nlpath=$(echo $wresultsfold | tr / "\n" | wc -l | cut -d \  -f1)
			if [ $nlpath -gt 1 ]
			then
				part1=$(echo $wresultsfold | cut -d / -f1-$(( $nlpath -1 )))
			fi
			part2=$(echo $wresultsfold | tr / "\n" | tail -1 | sed -re 's/\.dir//')_$indexnode.dir
			wresultsfold_node=$part1/$part2
			scp $l:$wresultsfold/* $lresultsdircond
			ssh $l "rm $wresultsfold_node/*"
		done
		
		sh $resultfold/formatresults $i $maxconds $resultdircond $lresultsfile

		if [ $verbose -gt 1 ]
		then
			echo "Results in $lresultsdircond formatted and added to $lresultsfile for the condsetsize $i"
			read input
		fi
		
		#remove/move
		if [ $keeplog -gt 0 ]
		then
			mv $lresultsdircond $listindeptested $listindep $logdir/independences.dir
		fi

	else
		echo "All parameters were found independent for cond set size < $i"
		break
	fi
done

##remove resources

if [ $verbose -gt 0 ]
then
	echo "Removing resources on remote machines (workers)"
fi

for l in $(cat $listnodes)
do

	ssh $l "rm -rf $wpathm"
	ssh $l "rm -rf $wresultsfold"
        ssh $l "rm -rf $wresourcesfold"
        ssh $l "rm -f $wpathds"
done



echo "*****************************************************"
echo "*                                                   *"
echo -e "\t\t Result saved in $lresultsfile"
echo "*                                                   *"
echo "*****************************************************"

#echo "Kill ssh process with ID $SSH_PROC_ID"
kill $SSH_PROC_ID
rm $listnodes
