Usage

centralnode.sh user@remotemachine remotedir_path listmachines.csv path_to_dataset_with_header.csv path_to_matlab_resources nloops subds_size maxcondsetsize alpha


Description

The script centralnode.sh takes a list of available machine, runing matlab, a dataset with p parameters and n samples. Given a maximum condition set size all the possible independences are tested and the tests are performed on the list of given machines.

This scripts assumes several things

1) All the workers have access to given directory mapped to their local filesystem
2) All workers have the same path to start matlab
3) After creating an ssh-agent the centralnode can access both the worker and the given directory by ssh w/o pswd

When the script is finished all the remote directories are removed and a csvfile with X,Y,Z1,...,Zm,alpha,N,l,perc is copied locally

There are several internal parameters that can be edited
verbose (default 1)
phasetest (default 0): if set to 1 then the remote files and results are kept on the remote machine
keeplog (default 1): if set to 1 every intermediary files/result is downloaded locally


#Example of cmd run for testing:
./centralnode.sh hours@xlan /datas/xlan/hours/package_causal_model_study.dir/test.dir listnodes_test /datas/xlan/hours/package_causal_model_study.dir/pack1.0/dataset_testing.csv /datas/xlan/hours/package_causal_model_study.dir/pack1.0/matlabsources.dir/ 2 30 2 0.05


Global scheme
0) Create ssh-agent
1) Create remotedir on shared machine (and logfiles dir if keeplog=1)
2) Copy resources (matlab files, scripts, dataset) on shared machine
3) Check the number of online machine
4) For each condset size
	i) Create a directory to store each indep test result
	ii) Generate list of independence to test (which implies removing the ones for which x,y were already found independent)
	iii) Launch the distributed tests and store result in dir a)
	iv) Format the result and put in a globalfile
5) Download the globalfile
6) If keeplog download all intermediary results
7) If phasetest = 1 remove intermedirary results
8) Remove matlab resources dir on remote host
9) Remove dataset on remote host
10) Kill ssh agent
