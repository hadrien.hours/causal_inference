function [] = selectTestedIndependences(pathin,pathout,threshold)
%This function read from csvfile the tested independences X indep Y cond Z
%and returns distinct X,Y independent
%Inputs
%       pthin: path to csvfile with list of all tested independences (at
%       least the format should have [x,y,percent]) assumes a header
%       pthout: path to csvfile to store list x,y independent (distinct)
%       threshold[optional]: percentage threshold to detect x,y as
%       independent (default 0.5)


if nargin < 2
    error('Wrong  number of args, see help');
elseif nargin == 2
    thr = 0.5;
elseif nargin == 3
    thr = threshold;
else
    error('Wrong number of args, see help');
end

listtested = csvread(pathin,1,0);

p = size(listtested,2);

if p < 3
    error('The list should have at least three columns: x,y, percentage')
end

xyp = listtested(:,[1,2,end]);

I = find(listtested(:,end) >= thr);

xyp2 = xyp(I,[1,2]);

xy_u = unique(xyp2,'rows');

%write the string to a file
fid = fopen(pathout,'w');
%fprintf('The identifier of %s is %d\n',filename,fid);
fprintf(fid,'x,y\r\n');
fclose(fid);

%% write the append the data to the file

%
% Call dlmwrite with a comma as the delimiter
%
dlmwrite(pathout,xy_u,'-append','delimiter',',');