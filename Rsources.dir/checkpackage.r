args = commandArgs(trailingOnly=TRUE)

pack=as.name(args[1])

#print(paste('Testing existence of package ',pack,sep=""))

packsinst <- installed.packages()

idx <- which(packsinst[,1] == pack)

if (length(idx) > 0){
	print(paste('Package ',pack,' installed',sep=""))
} else
	print(paste('Package ',pack,' absent',sep=""))
