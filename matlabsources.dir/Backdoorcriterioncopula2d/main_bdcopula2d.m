function [x,fx,ex,sx,mx,ex2,sx2] = main_bdcopula2d(ds,dimx,dimy,valy,deltay,dimz,N,methodk,methodc)
%This function estimated the probability density function of X after
%intervention on Y given the back door variable Z
%This function is restricted to the case where dim(Y) = 1 and dim(Z) = 2
%Inputs
%       ds: the dataset
%       dimx: The X dimension
%       dimy: The Y dimension
%       valy: The value of Y intervention
%       deltay: The interval width around valy for estimation
%       dimz: The Z dimensions
%       N: The number of points for the support of marginals and pdfs
%       methodk: The kernel type [default normal]
%       methodc: The copula type [default t]
%Outputs
%       x: value of x
%       fx: probabilyt of X=x after intervention
%       ex: expected value post intervention
%       sx: standard deviation of x post intervention

n = size(ds,1);
p = size(ds,2);

if nargin < 7
    error('Not enough args, see help');
elseif nargin == 7
    methodk = 'normal';
    methodc = 't';
elseif nargin == 8
    methodc = 't';
elseif nargin > 9
    error('Too many args, see help');
end

if size(dimz,2) ~= 2
    error('conditionning set must be of size 2');
end

vyu = valy+deltay/2;
vyl = valy-deltay/2;

Iy = find(ds(:,dimy) <= vyu & ds(:,dimy) >= vyl);

fprintf('%d samples found for the given interval [%.2g,%.2g]\n',size(Iy,1),vyl,vyu);

if size(Iy,1) == 0
    error('No value found for the given interval around Y')
end

X = ds(Iy,dimx);
Z = ds(Iy,dimz);

%Compute pre intervention metrics
mx = mean(X);
xs = linspace(min(X),max(X),N);
PDFx = ksdensity(X,xs,'kernel',methodk,'function','pdf');
hx = @(t) interp1(xs,PDFx.*xs,t,'cubic');
ex2 = integral(hx,min(xs),max(xs));
hx2 = @(t) interp1(xs,xs.^2.*PDFx,t,'cubic');
m2 = integral(hx2,min(xs),max(xs));
sx2 = sqrt(m2-ex2^2);

fprintf('Start computing the bivariate pdf of Z\n');
[fzl,zl] = computemultidimprob_linext(Z,[1,2],N,methodk,methodc);
fprintf('Start computing the conditional pdf of X given Z for Y value %.2g\n',valy);
[fxczl,xzl] = computecondprob2d([X,Z],1,[2,3],N,methodk,methodc);
fprintf('Start integrating the Back Door formula\n');
fprintf('The size of the conditional pdf is %d,%d\n',size(fxczl,1),size(fxczl,2));
fprintf('The size of the bivariate pdf is %d,%d\n',size(fzl,1),size(fzl,2));
fprintf('The size of the conditional pdf support is %d,%d\n',size(xzl,1),size(xzl,2));
fprintf('The size of the bivariate pdf support is %d,%d\n',size(zl,1),size(zl,2));
[fx,x] = computebdintegration2drectangle(fxczl,fzl,xzl,zl);
fprintf('Finished estimating the post interventional distribution\n');
h = @(t) interp1(x,x.*fx,t,'cubic');
ex = integral(h,min(x),max(x));
h2 = @(t) interp1(x,x.^2.*fx,t,'cubic');
m2 = integral(h2,min(x),max(x));
sx = sqrt(m2 - ex^2);

