function [Pxz,supportpdfxz_x] = computecondprob2d(ds,dimx,dimz,N,methodk,methodc)
%This function estimated the condtional probability density function of X 
%conditionning on Z
%This function is restricted to the case where dim(Z) = 2
%Inputs
%       ds: the dataset
%       dimx: The X dimension
%       dimz: The Z dimensions
%       N: The number of points for the support of marginals and pdfs
%       methodk: The kernel type [default normal]
%       methodc: The copula type [default t]
%Outputs
%       xzl: value of x and z
%       fx: probabilyt of X=x conditioned on Z=z

n = size(ds,1);
p = size(ds,2);

if nargin < 4
    error('Not enough args, see help');
elseif nargin == 4
    methodk = 'normal';
    methodc = 't';
elseif nargin == 5
    methodc = 't';
elseif nargin > 6
    error('Too many args, see help');
end

if size(dimz,2) ~= 2
    error('conditionning set must be of size 2');
end

X = ds(:,dimx);
Z = ds(:,dimz);

%compute marginals
margincx_1 = zeros(1,n);
supportx_1 = X';
margincx_2= zeros(1,N);
%use linearly spaced point support
supportx_2 = linspace(min(X),max(X),N);
margincz_1 = zeros(2,n);
supportz_1 = Z';
margincz_2 = zeros(2,N);
supportz_2 = zeros(2,N);
%use linearly spaced point support
for i = 1:2
    supportz_2(i,:) = linspace(min(Z(:,i)),max(Z(:,i)),N);
end
    %compute CDF marginals for copula parameters estimations
    fprintf('Starting computing marginal for CDF of X\n');
margincx_1 = ksdensity(supportx_1,supportx_1,'kernel',methodk,'function','cdf');
margincx_1(margincx_1==0) = eps;
margincx_1(margincx_1==1) = 1-eps;
    fprintf('Starting computing marginals for CDFs of Z\n');
for i = 1:2
    margincz_1(i,:) = ksdensity(supportz_1(i,:),supportz_1(i,:),'kernel',methodk,'function','cdf');
end
    %remove extremes for copulafit
margincz_1(margincz_1==0) = eps;
margincz_1(margincz_1==1) = 1-eps;
    %compute CDF marginals for density support
    fprintf('Compute marginal for CDF of X for linearly spaced point for the support\n');
margincx_2= ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','cdf');
%remove extremes for copulafit
margincx_2(margincx_2==0) = eps;
margincx_2(margincx_2==1) = 1-eps;
    fprintf('Compute marginals for CDFs of Z for linearly spaced point for the support\n');
for i = 1:2
    margincz_2(i,:) = ksdensity(supportz_1(i,:),supportz_2(i,:),'kernel',methodk,'function','cdf');%use linearly spaced point support
end
%remove extremes for copulafit
margincz_2(margincz_2==0) = eps;
margincz_2(margincz_2==1) = 1-eps;

%Compute bivariate CDF of Z
fprintf('Starting computing the bivariate CDF of Z\n');
if strcmp(methodc,'t')
    [rhoz,nuz] = copulafit('t',margincz_1');
    Fz1 = copulacdf('t',margincz_1',rhoz,nuz);
    fprintf('The bivariate CDF of Z was computed for estimating the bivariate copula of C_x/z(npoints = %d)\n',size(Fz1,1));
    [Zyg1,Zyg2] = meshgrid(margincz_2(1,:)',margincz_2(2,:)');%meshgrid of Z CDFs
    [Zxg1,Zxg2] = meshgrid(supportz_2(1,:)',supportz_2(2,:)');%meshgrid of Z values
    fprintf('Starting computing the bivariate CDF of Z for equally 2D spaced points (npoints = %d)\n',size(Zyg1(:),1))
    Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:)],rhoz,nuz);
    fprintf('The bivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
elseif strcmp(methodc,'Gaussian')
    rhoz = copulafit('Gaussian',margincz_1');
    Fz1 = copulacdf('Gaussian',margincz_1',rhoz);
    fprintf('The bivariate CDF of Z was computed for estimating the bivariate copula of C_x/z(npoints = %d)\n',size(Fz1,1));
    [Zyg1,Zyg2] = meshgrid(margincz_2(1,:)',margincz_2(2,:)');%meshgrid of Z CDFs
    [Zxg1,Zxg2] = meshgrid(supportz_2(1,:)',supportz_2(2,:)');%meshgrid of Z values
    fprintf('Starting computing the bivariate CDF of Z for equally 2D spaced points (npoints = %d)\n',size(Zyg1(:),1))
    Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:)],rhoz);
    fprintf('The bivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
end

%Compute bivariate PDF of X conditionally on Z
    %compute x pdf
marginpx_2 = ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','pdf');
fprintf('Finished computing the marginal pdf of X for conditional PDF computing of f_x/z (npoints = %d)\n',size(marginpx_2,1));
    %Estimate copula
if strcmp(methodc,'t')
    %estimate parameters with CDFs of samples points
    [rhoxz,muxz] = copulafit('t',[margincx_1',Fz1]);
    fprintf('Parameters computed for the bivariate copula of c_x,z (mu = %.2g, rhoxz of size %d,%d)\n',muxz,size(rhoxz,1),size(rhoxz,2));
    %compute bivariate pdfs at the meshgrid points
    supportcdfx = zeros(N^3,1);
    supportpdfx = zeros(N^3,1);
    supportcdfz = zeros(N^3,1);
    supportpdfxz_x = zeros(N^3,3);
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    for i = 1:N%%%%%%% TO BE IMPROVED
        %fprintf('Trying to fit a vector of size %d in another of size %d for indices between %d and %d\n',size(ones(N^2,1)*margincx_2(i),1),size(supportcdfx((i-1)*N^2+1:i*N^2),1),1+(i-1)*N^2,i*N^2)%debug
        supportcdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*margincx_2(i);
        supportpdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*marginpx_2(i);
        supportcdfz(1+(i-1)*N^2:i*N^2) = Fz2;
        supportpdfxz_x(1+(i-1)*N^2:i*N^2,1) = ones(N^2,1)*supportx_2(i);
        supportpdfxz_x(1+(i-1)*N^2:i*N^2,[2,3]) = [Zxg1(:),Zxg2(:)];
    end
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^3);
    Cxz = copulapdf('t',[supportcdfx,supportcdfz],rhoxz,muxz);
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
elseif strcmp(method,'Gaussian')
    %estimate parameters with CDFs of samples points
    rhoxz = copulafit('t',[margincx_1',Fz1]);
    fprintf('Parameters computed for the bivariate copula of c_x,z (rhoxz of size %d,%d)\n',size(rhoxz,1),size(rhoxz,2));
    %compute bivariate pdfs at the meshgrid points
    supportcdfx = zeros(N^3,1);
    supportpdfx = zeros(N^3,1);
    supportcdfz = zeros(N^3,1);
    supportpdfxz_x = zeros(N^3,3);
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    for i = 1:N%%%%%%% TO BE IMPROVED
        supportcdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*margincx_2(i);
        supportpdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*marginpx_2(i);
        supportcdfz(1+(i-1)*N^2:i*N^2) = Fz2;
        supportpdfxz_x(1+(i-1)*N^2:i*N^2,1) = ones(N^2,1)*supportx_2(i);
        supportpdfxz_x(1+(i-1)*N^2:i*N^2,[2,3]) = [Zxg1(:),Zxg2(:)];
    end
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^3);
    Cxz = copulapdf('Gaussian',[supportcdfx,supportcdfz],rhoxz);
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
end