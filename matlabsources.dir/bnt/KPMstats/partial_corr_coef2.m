function [r] = partial_corr_coef2(D, i, j, k)
%compute correlation coefficient with linear regression

N = size(D,1);
if ~isempty(k)
    %k
    w1 = regress(D(:,i),D(:,k));
    w2 = regress(D(:,j),D(:,k));
end

if ~isempty(k)
    %w1
    r1 = D(:,i) - w1.*D(:,k);
    r2 = D(:,j) - w2.*D(:,k);
else
    r1 = D(:,i);
    r2 = D(:,j);
end

%n = (N*sum(r1.*r2)-sum(r1)*sum(r2))
%d = (sqrt(N*sum(r1.^2))-sum(r1)^2*sqrt(N*sum(r2.^2)-sum(r2)^2))


r = (N*sum(r1.*r2)-sum(r1)*sum(r2))/(sqrt(N*sum(r1.^2))-sum(r1)^2*sqrt(N*sum(r2.^2)-sum(r2)^2));