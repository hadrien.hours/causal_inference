function [s,p] = hilbert_schmidt_indep_test(X,Y,Z,ds,alpha,shuffles)
% This function is using Arthur Gretton hsic implementation to fit in the
% pc algorithm as independence test
% INPUTS
%       ds = n*p dataset
%       X = dim of first var
%       Y = dim of second var
%       Z = dim of cond var
%       alpha = scalar, significance level
%       shuffles = number of permutation to test null distribution
if isempty(Z)
    %fprintf('Entered in empty Z indep test\n');
   [s0,p] = hsicTestBootIC(ds(:,X),ds(:,Y),alpha,shuffles);
   if ~s0
      fprintf('%d independent of %d\n',X,Y); 
%    else
%       fprintf('%d dependent of %d\n',X,Y); 
   end
else
   [s0,p] = hsiccondTestIC(ds(:,X),ds(:,Y),ds(:,Z),alpha,shuffles);
   if ~s0
      fprintf('%d independent of %d conditional on %d\n',X,Y,Z); 
%    else
%       fprintf('%d dependent of %d conditional on %d\n',X,Y,Z); 
   end
end
s = ~s0;