function [fxcz,xcz] = condprob3d(ds,dimx,dimz,N,methodk,methodc)
%This function makes use of kernel and compula to compute the pdf of x
%conditionning on y
%Inputs
%       ds: dataset
%       dimx: dimension of x
%       dimy: dimension(s) of y
%       N the number of points for support
%       methodk [optional] = kernel type [default normal];
%       methodc [optional] = copula type [default t]
%Outputs
%       fxczl : pdf of x/z
%       xzl: values of x,z

n = size(ds,1);
p = size(ds,2);


if nargin < 4
    error('Not enough args, see help');
elseif nargin == 4
    methodk = 'normal';
    methodc = 't';
elseif nargin == 5
    methodc = 't';
elseif nargin > 6
    error('Too many args, see help');
end

dz = size(dimz,2);


if dz ~= 3
    error('This function function is for conditionning set of size 3');
end

%Compute the trididimentional CDF of Z
fprintf('Start computation of tridimensional CDF of Z\n');
    %Start by computing unidimensional CDF
supportcdfz_1 = ds(:,dimz);%on sample points for estimating copula pars
supportcdfz_2 = zeros(N,3);%on equally spaced point for generating final pdf
for i = 1:3
    supportcdfz_2(:,i) = linspace(min(ds(:,dimz(i))),max(ds(:,dimz(i))),N)';
end
cdfz1 = zeros(n,3);
cdfz2 = zeros(N,3);
for i = 1:3
    cdfz1(:,i) = ksdensity(supportcdfz_1(:,i),supportcdfz_1(:,i),'kernel',methodk,'function','cdf');
    cdfz2(:,i) = ksdensity(supportcdfz_1(:,i),supportcdfz_2(:,i),'kernel',methodk,'function','cdf');
end
fprintf('\t Finished computing the different marginals of Z\n');
    %Copmute the trivariate with copula and CDFs
if strcmp(methodc,'t')
    [rhoz,nuz] = copulafit('t',cdfz1);%pars estimated with CDFs on sample points
    Fz1 = copulacdf('t',cdfz1,rhoz,nuz);
    [Zg1,Zg2,Zg3] = ndgrid(cdfz2(:,1),cdfz2(:,2),cdfz2(:,3));
    fprintf('\t Start computing trivariate CDF of Z, might take some time (%d points to compute)\n',size(Zg1(:),1))
    Fz2 = copulacdf('t',[Zg1(:),Zg2(:),Zg3(:)],rhoz,nuz);%Create Fz on 3D grid
elseif strcmp(methodc,'Gaussian')
    rhoz = copulafit('Gaussian',cdfz1);
    Fz1 = copulacdf('t',cdfz1,rhoz,nuz);
    [Zg1,Zg2,Zg3] = ndgrid(cdfz2(:,1),cdfz2(:,2),cdfz2(:,3));
    fprintf('\t Start computing trivariate CDF of Z, might take some time (%d points to compute)\n',size(Zg1(:),1))
    Fz2 = copulacdf('t',[Zg1(:),Zg2(:),Zg3(:)],rhoz,nuz);
end
fprintf('\t Finished computing the trivariate CDF of Z\n')
fprintf('Start computing the conditional pdf of X/Z\n');
%Compute cond Fx/z = Cx,z(Fx,Fz)*fx
    %Univariate Fx on sample points for copula estimate
fprintf('\t Start computing the CDFs and PDFs marginals for X on both samples and linearly spaced points\n');
supportcdfx_1 = ds(:,dimx);
supportcdfx_2 = linspace(min(ds(:,dimx)),max(ds(:,dimx)),N)';
cdfx1 = ksdensity(supportcdfx_1,supportcdfx_1,'kernel',methodk,'function','cdf');
cdfx2 = ksdensity(supportcdfx_1,supportcdfx_2,'kernel',methodk,'function','cdf');
pdfx2 = ksdensity(supportcdfx_1,supportcdfx_2,'kernel',methodk,'function','pdf');
    fprintf('\t Finished computing PDF and CDFs of X\n')
    %Bivariate copula of X and Z (Z being tridimensional)
if strcmp(methodc,'t')
        %Estimate copula parameter with CDFs on sample points
    [rhoxz,nuxz] = copulafit('t',[Fz1,cdfx1]);
    fprintf('\t Generate the 4D grid of Z1,Z2,Z3,X points on which the cond PDF will be defined\n');
    fprintf('\t Sizes of supports for z are %d %d %d and for x %d\n',size(supportcdfz_2(:,1),1),size(supportcdfz_2(:,2),1),size(supportcdfz_2(:,3),1),size(supportcdfx_2(:,1),1))
    [Zgx1,Zgx2,Zgx3,Xgx] = ndgrid(supportcdfz_2(:,1),supportcdfz_2(:,2),supportcdfz_2(:,3),supportcdfx_2(:,1));
        %Generate the CDFs points corresponding to the supports for 4D
        % Not that ndgrid generate a grid on W,X,Y,Z s.t
        % [W(:),X(:),Y(:),Z(:)] = w1 x1 y1 z1
        %                         w2 x1 y1 z1
        %                           .......
        %                         wn x1 y1 z1
        %                         w1 x2 y1 z1
        %                         w2 x2 y1 z1
        %                           ........
        %                         wn xn y1 z1
        %                         w1 x1 y2 z1
        %                           ........
        %                         wn xn yn zn   (1)
    dz = size(Fz2,1);%N^3
    dx = size(cdfx2,1);%N
    fprintf('\t Size of trivariate CDF of Z is %d\n',dz)
    fprintf('\t Size of univariate CDF of X is %d\n',dx)
% % % % % %     Zv = diag(Fz2)*ones(dz,dx);%gives a Matrix of size dz*dx where lines are Fz2(i).....Fz2(i) and columns are Fz2(1),Fz2(2),.....,Fz2(dz);
% Correct %     Xv = ones(dz,dx)*diag(cdfx2);%gives a Matrix of size dz*dx where lines are cdfx(1),...cdfx(dx) and columns are cdfx(j).....cdfx(j)
%   but   %     fxv = ones(dz,dx)*diag(pdfx2);%same as before with PDF
% out of  %
% memory  %
% problem %
% % % % % %
    Fz2_1 = Fz2*ones(1,dx);%Matrix repeating dx columns being Fz2(1)..... Fz2(dz)
    Zv = Fz2_1(:);
    Xv_1 = ones(dz,1)*cdfx2';%Martix repeating dz lines being cdfx(1).....cdfx(dx)
    Xv = Xv_1(:);
    fxv = ones(dz,1)*pdfx2';%Martix repeating dz lines being pdfx(1).....pdfx(dx)
    fxvl = fxv(:);
    fprintf('\t After creating corresponding CDF for ngrid on Z and X, their corresponding sizes are %d and %d\n',size(Zv(:),1),size(Xv(:),1))
    CDFxCz = [Zv(:),Xv];%output the matrix of the CDFs correponding to (1) knowing that Fz represents the trivariate CDF of Z
    if size(CDFxCz,1) ~= size(Zgx1(:),1)%Sanity check
        error('Dimension mismatching while creating cond CDF supports and cond CDF values (CDF %d, Zg1 %d)',size(CDFxCz,1),size(Zgx1(:),1))
    end
    %fprintf('\t Finished generating the corresponding bivariate CDF of Z,X (npoints = %d) \n',size(CDFxCz,1));
    fprintf('\t Start computing bivariate PDF of X/Z, might take some time (%d points to compute)\n',size(CDFxCz,1))
    fxcz = copulapdf('t',CDFxCz,rhoxz,nuxz).*fxvl;
    fprintf('\t Finished computing the conditional PDF of X/Z\n')
elseif strcmp(methodc,'Gaussian')
        %Estimate copula parameter with CDFs on sample points
    rhoxz = copulafit('Gaussian',[Fz1,cdfx1]);
    fprintf('\t Generate the 4D grid of Z1,Z2,Z3,X points on which the cond PDF will be defined\n');
    [Zgx1,Zgx2,Zgx3,Xgx] = ndgrid(supportcdfz_2(:,1),supportcdfz_2(:,2),supportcdfz_2(:,3),supportcdfx_2(:,1));
        %Generate the CDFs points corresponding to the supports for 4D
        % Not that ndgrid generate a grid on W,X,Y,Z s.t
        % [W(:),X(:),Y(:),Z(:)] = w1 x1 y1 z1
        %                         w2 x1 y1 z1
        %                           .......
        %                         wn x1 y1 z1
        %                         w1 x2 y1 z1
        %                         w2 x2 y1 z1
        %                           ........
        %                         wn xn y1 z1
        %                         w1 x1 y2 z1
        %                           ........
        %                         wn xn yn zn   (1)
    dz = size(Fz2,1);%N^3
    dx = size(cdfx2,1);%N
    fprintf('\t Size of trivariate CDF of Z is %d\n',dz)
% % % % % %     Zv = diag(Fz2)*ones(dz,dx);%gives a Matrix of size dz*dx where lines are Fz2(i).....Fz2(i) and columns are Fz2(1),Fz2(2),.....,Fz2(dz);
% Correct %     Xv = ones(dz,dx)*diag(cdfx2);%gives a Matrix of size dz*dx where lines are cdfx(1),...cdfx(dx) and columns are cdfx(j).....cdfx(j)
%   but   %     fxv = ones(dz,dx)*diag(pdfx2);%same as before with PDF
% out of  %
% memory  %
% problem %
% % % % % %
    Fz2_1 = Fz2*ones(1,dx);%Matrix repeating dx columns being Fz2(1)..... Fz2(dz)
    Zv = Fz2_1(:);
    Xv_1 = ones(dz,1)*cdfx2';%Martix repeating dz lines being cdfx(1).....cdfx(dx)
    Xv = Xv_1(:);
    fxv = ones(dz,1)*pdfx2';%Martix repeating dz lines being pdfx(1).....pdfx(dx)
    fxvl = fxv(:);
    CDFxCz = [Zv(:),Xv];%output the matrix of the CDFs correponding to (1) knowing that Fz represents the trivariate CDF of Z
    if size(CDFxCz,1) ~= size(Zgx1(:),1)%Sanity check
        error('Dimension mismatching while creating cond CDF supports and cond CDF values')
    end
    %fprintf('\t Finished generating the corresponding bivariate CDF of Z,X (npoints = %d) \n',size(CDFxCz,1));
    fprintf('\t Start computing bivariate PDF of X/Z, might take some time (%d points to compute)\n',size(CDFxCz,1))
    fxcz = copulapdf('Gaussian',CDFxCz,rhoxz).*fxvl;
    fprintf('\t Finished computing the conditional PDF of X/Z\n')
end

xcz = [Zgx1(:),Zgx2(:),Zgx3(:),Xgx(:)];