function [fxcyl,xyl] = computecondprob3d(ds,dimx,dimy,N,methodk,methodc)
%This function makes use of kernel and compula to compute the pdf of x
%conditionning on y
%Inputs
%       ds: dataset
%       dimx: dimension of x
%       dimy: dimension(s) of y
%       N the number of points for support
%       methodk [optional] = kernel type [default normal];
%       methodc [optional] = copula type [default t]
%Outputs
%       fxcyl : pdf of x/y
%       xyl: values of x,y

n = size(ds,1);
p = size(ds,2);

methodgrid = 2;

if nargin < 4
    error('Not enough args, see help');
elseif nargin == 4
    methodk = 'normal';
    methodc = 't';
elseif nargin == 5
    methodc = 't';
elseif nargin > 6
    error('Too many args, see help');
end

dz = size(dimz,2);

%compute marginals
margincx_1 = zeros(1,n);
supportx_1 = X';
margincx_2= zeros(1,N);
%use linearly spaced point support
supportx_2 = linspace(min(X),max(X),N);
margincz_1 = zeros(dz,n);
supportz_1 = Z';
margincz_2 = zeros(dz,N);
supportz_2 = zeros(dz,N);
%use linearly spaced point support
for i = 1:dz
    supportz_2(i,:) = linspace(min(Z(:,i)),max(Z(:,i)),N);
end
    %compute CDF marginals for copula parameters estimations
    fprintf('Starting computing marginal for CDF of X\n');
margincx_1 = ksdensity(supportx_1,supportx_1,'kernel',methodk,'function','cdf');
margincx_1(margincx_1==0) = eps;
margincx_1(margincx_1==1) = 1-eps;
    fprintf('Starting computing marginals for CDFs of Z\n');
for i = 1:dz
    margincz_1(i,:) = ksdensity(supportz_1(i,:),supportz_1(i,:),'kernel',methodk,'function','cdf');
end
    %remove extremes for copulafit
margincz_1(margincz_1==0) = eps;
margincz_1(margincz_1==1) = 1-eps;
    %compute CDF marginals for density support
    fprintf('Compute marginal for CDF of X for linearly spaced point for the support\n');
margincx_2= ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','cdf');
%remove extremes for copulafit
margincx_2(margincx_2==0) = eps;
margincx_2(margincx_2==1) = 1-eps;
    fprintf('Compute marginals for CDFs of Z for linearly spaced point for the support\n');
for i = 1:dz
    margincz_2(i,:) = ksdensity(supportz_1(i,:),supportz_2(i,:),'kernel',methodk,'function','cdf');%use linearly spaced point support
end
%remove extremes for copulafit
margincz_2(margincz_2==0) = eps;
margincz_2(margincz_2==1) = 1-eps;

%Compute bivariate CDF of Z
fprintf('Starting computing the multivariate CDF of Z\n');
if strcmp(methodc,'t')
    [rhoz,nuz] = copulafit('t',margincz_1');
    Fz1 = copulacdf('t',margincz_1',rhoz,nuz);
    fprintf('The bivariate CDF of Z was computed for estimating the bivariate copula of C_x/z(npoints = %d)\n',size(Fz1,1));
    if dz == 2
        if methodgrid == 2
            [Zyg1,Zyg2] = meshgrid(margincz_2(1,:)',margincz_2(2,:)');%meshgrid of Z CDFs
            [Zxg1,Zxg2] = meshgrid(supportz_2(1,:)',supportz_2(2,:)');%meshgrid of Z values
        elseif methodgrid == 1
            [Zyg1,Zyg2] = ndgrid(margincz_2(1,:)',margincz_2(2,:)');%ndgrid of Z CDFs
            [Zxg1,Zxg2] = ndgrid(supportz_2(1,:)',supportz_2(2,:)');%ndgrid of Z values !! values ordered differently with ndgrid
        end
        fprintf('Starting computing the bivariate CDF of Z for equally 2D spaced points (npoints = %d)\n',size(Zyg1(:),1))
        Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:)],rhoz,nuz);
        fprintf('The bivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
    elseif dz == 3      
        [Zyg1,Zyg2,Zyg3] = ndgrid(margincz_2(1,:)',margincz_2(2,:)',margincz_2(3,:)');%ndgrid of Z CDFs
        %[Zxg1,Zxg2,Zxg3] = ndgrid(supportz_2(1,:)',supportz_2(2,:)',supportz_2(3,:));%ndgrid of Z values
        fprintf('Starting computing the bivariate CDF of Z for equally 3D spaced points (npoints = %d)\n',size(Zyg1(:),1))
        Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:),Zyg3(:)],rhoz,nuz);
        fprintf('The trivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
    else
        error('Method not computed yet for conditionning set of size greater than 3')
    end
elseif strcmp(methodc,'Gaussian')
    rhoz = copulafit('Gaussian',margincz_1');
    Fz1 = copulacdf('Gaussian',margincz_1',rhoz);
    fprintf('The bivariate CDF of Z was computed for estimating the bivariate copula of C_x/z(npoints = %d)\n',size(Fz1,1));
    if  dx == 2
        [Zyg1,Zyg2] = meshgrid(margincz_2(1,:)',margincz_2(2,:)');%meshgrid of Z CDFs
        [Zxg1,Zxg2] = meshgrid(supportz_2(1,:)',supportz_2(2,:)');%meshgrid of Z values
        fprintf('Starting computing the bivariate CDF of Z for equally 2D spaced points (npoints = %d)\n',size(Zyg1(:),1))
        Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:)],rhoz);
        fprintf('The bivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
    elseif dx == 3
        [Zyg1,Zyg2,Zyg3] = ndgrid(margincz_2(1,:)',margincz_2(2,:)',margincz_2(3,:)');%meshgrid of Z CDFs
        [Zxg1,Zxg2] = meshgrid(supportz_2(1,:)',supportz_2(2,:)',supportz_2(3,:));%meshgrid of Z values
        fprintf('Starting computing the bivariate CDF of Z for equally 3D spaced points (npoints = %d)\n',size(Zyg1(:),1))
        Fz2 = copulacdf('t',[Zyg1(:),Zyg2(:),Zyg3(:)],rhoz);
        fprintf('The trivariate CDF of Z was computed for linearly spaced points on grid (npoints = %d)\n',size(Fz2,1));
    else
        error('Method not computed yet for conditionning set of size greater than 3')
    end        
end

%Compute bivariate PDF of X conditionally on Z
    %compute x pdf
marginpx_2 = ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','pdf');
fprintf('Finished computing the marginal pdf of X for conditional PDF computing of f_x/z (npoints = %d)\n',size(marginpx_2,1));
    %Estimate copula
if strcmp(methodc,'t')
    %estimate parameters with CDFs of samples points
    [rhoxz,muxz] = copulafit('t',[margincx_1',Fz1]);
    fprintf('Parameters computed for the bivariate copula of c_x,z (mu = %.2g, rhoxz of size %d,%d)\n',muxz,size(rhoxz,1),size(rhoxz,2));
    %compute multivariate pdfs at the meshgrid points
    supportcdfx = zeros(N^(dz+1),1);
    supportpdfx = zeros(N^(dz+1),1);
    supportcdfz = zeros(N^(dz+1),1);
    supportpdfxz_x = zeros(N^(dz+1),dz+1);
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    if methodgrid == 1
        dimzv = N^dz;
        dimxv = N;
        Ac = ones(dimzv,dimxv)*diag(margincx_2);
        Ap = ones(dimzv,dimxv)*diag(marginpx_2);
        Bc = diag(Fz2)*ones(dimzv,dimxv);
        supportcdfx = Ac(:);%ordered as N^2 times CDFx(1) followed by N^2 times CDFx(2),etc....
        supportpdfx = Ap(:);%ordered as N^2 times PDFx(1) followed by N^2 times PDF(2),etc....
        supportcdfz = Bc(:);%ordered as CDFz(1),CDFz(2)....CDFz(N^2), n times, CDFz ordered so that y11,y12,y13 y21,y12,y13......y1n,y12,y13 y11,y22,y13...
        if dz == 2
            [Zxg1,Zxg2,Xg] = ndgrid(supportz_2(1,:)',supportz_2(2,:)',supportx_2');%ngrid repeat once every value for first dim, N times for second, N^2 for third... etc opposite of meshgrid !
            supportpdfxz_x = [Zxg1(:),Zxg2(:),Xg(:)];
        elseif dz == 3
            [Zxg1,Zxg2,Zxg3,Xg] = ndgrid(supportz_2(1,:)',supportz_2(2,:)',supportz_2(3,:),supportx_2');
            supportpdfxz_x = [Zxg1(:),Zxg2(:),Zxg3(:),Xg(:)];
    elseif methodgrid == 2
        if dz == 2
            for i = 1:N%%%%%%% TO BE IMPROVED
                %fprintf('Trying to fit a vector of size %d in another of size %d for indices between %d and %d\n',size(ones(N^2,1)*margincx_2(i),1),size(supportcdfx((i-1)*N^2+1:i*N^2),1),1+(i-1)*N^2,i*N^2)%debug
                supportcdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*margincx_2(i);
                supportpdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*marginpx_2(i);
                supportcdfz(1+(i-1)*N^2:i*N^2) = Fz2;
                supportpdfxz_x(1+(i-1)*N^2:i*N^2,1) = ones(N^2,1)*supportx_2(i);
                supportpdfxz_x(1+(i-1)*N^2:i*N^2,[2,3]) = [Zxg1(:),Zxg2(:)];
            end
        elseif dz == 3
            for i = 1:N%%%%%%% TO BE IMPROVED
                %fprintf('Trying to fit a vector of size %d in another of size %d for indices between %d and %d\n',size(ones(N^2,1)*margincx_2(i),1),size(supportcdfx((i-1)*N^2+1:i*N^2),1),1+(i-1)*N^2,i*N^2)%debug
                supportcdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*margincx_2(i);
                supportpdfx(1+(i-1)*N^2:i*N^2) = ones(N^2,1)*marginpx_2(i);
                supportcdfz(1+(i-1)*N^2:i*N^2) = Fz2;
                supportpdfxz_x(1+(i-1)*N^2:i*N^2,1) = ones(N^2,1)*supportx_2(i);
                supportpdfxz_x(1+(i-1)*N^2:i*N^2,[2,3]) = [Zxg1(:),Zxg2(:)];
            end
        else
            error('Method not yet implemented for conditionning set greater than 3 vars');
        end
    end
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^3);
    if methodgrid == 2
        Cxz = copulapdf('t',[supportcdfx,supportcdfz],rhoxz,muxz);
    elseif methodgrid == 1
        Cxz = copulapdf('t',[supportcdfz,supportcdfx],rhoxz,muxz);
    end
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
elseif strcmp(method,'Gaussian')
    %estimate parameters with CDFs of samples points
    rhoxz = copulafit('t',[margincx_1',Fz1]);
    fprintf('Parameters computed for the bivariate copula of c_x,z (rhoxz of size %d,%d)\n',size(rhoxz,1),size(rhoxz,2));
    %compute bivariate pdfs at the meshgrid points
    supportcdfx = zeros(N^3,1);
    supportpdfx = zeros(N^3,1);
    supportcdfz = zeros(N^3,1);
    supportpdfxz_x = zeros(N^3,3);
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    for i = 1:N%%%%%%% TO BE IMPROVED
        supportcdfx(1+(i-1)*N^dz:i*N^dz) = ones(N^dz,1)*margincx_2(i);
        supportpdfx(1+(i-1)*N^dz:i*N^dz) = ones(N^dz,1)*marginpx_2(i);
        supportcdfz(1+(i-1)*N^dz:i*N^zd) = Fz2;
        supportpdfxz_x(1+(i-1)*N^dz:i*N^dz,1) = ones(N^dz,1)*supportx_2(i);
        supportpdfxz_x(1+(i-1)*N^dz:i*N^dz,[    2,3]) = [Zxg1(:),Zxg2(:)];
    end
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^3);
    Cxz = copulapdf('Gaussian',[supportcdfx,supportcdfz],rhoxz);
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
end