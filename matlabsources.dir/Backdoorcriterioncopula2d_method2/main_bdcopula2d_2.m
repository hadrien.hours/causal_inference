function [x,fx,ex,sx] = main_bdcopula2d_2(ds,dimx,dimy,valy,deltay,dimz,N,methodk,methodc)
%This function estimated the probability density function of X after
%intervention on Y given the back door variable Z
%This function is restricted to the case where dim(Y) = 1 and dim(Z) = 2
%Inputs
%       ds: the dataset
%       dimx: The X dimension
%       dimy: The Y dimension
%       valy: The value of Y intervention
%       deltay: The interval width around valy for estimation
%       dimz: The Z dimensions
%       N: The number of points for the support of marginals and pdfs
%       methodk: The kernel type [default normal]
%       methodc: The copula type [default t]
%Outputs
%       x: value of x
%       fx: probabilyt of X=x after intervention
%       ex: expected value post intervention
%       sx: standard deviation of x post intervention

n = size(ds,1);
p = size(ds,2);

if nargin < 7
    error('Not enough args, see help');
elseif nargin == 7
    methodk = 'normal';
    methodc = 't';
elseif nargin == 8
    methodc = 't';
elseif nargin > 9
    error('Too many args, see help');
end

if size(dimz,2) ~= 2
    error('conditionning set must be of size 2');
end

vyu = valy+deltay/2;
vyl = valy-deltay/2;

Iy = find(ds(:,dimy) <= vyu & ds(:,dimy) >= vyl);

fprintf('%d samples found for the given interval [%.2g,%.2g]\n',size(Iy,1),vyl,vyu);

X = ds(:,dimx);
Y = ds(:,dimy);
Z = ds(:,dimz);

if size(Iy,1) == 0
    error('No value found for the given interval around Y')
end

fprintf('Start computing the bivariate pdf of Z\n');
[fzl,zl] = computemultidimprob_linext(Z,[1,2],N,methodk,methodc);
fprintf('Start computing the conditional pdf of X given Z for Y value %.2g\n',valy);
[fxcyzl,yzxl] = condprob3d([X,Y,Z],1,[2,3,4],N,methodk,methodc);
fprintf('Conditioning on parameter %d value %.2g\n',dimy,valy);
% Yc = yzxl(:,1);
% Iy = find(Yc<=vyu & Yc >= vyl);
% fprintf('Number of values found is %d (among %d)\n',size(Iy,1),size(Yc,1));
% %Once conditionned on Y, only keep fx/z
% fxczl = fxcyzl(Iy);
% zxl = yzxl(Iy,[2:end]);
[fxczl,zxl] = findyval(vyl,vyu,fxcyzl,yzxl);
[fx,x] = computebdintegration2drectangle_2(fxczl,fzl,zxl,zl);
fprintf('Finished estimating the post interventional distribution\n');
h = @(t) interp1(x,x.*fx,t,'cubic');
ex = integral(h,min(x),max(x));
h2 = @(t) interp1(x,x.^2.*fx,t,'cubic');
m2 = integral(h2,min(x),max(x));
sx = sqrt(m2 - ex^2);