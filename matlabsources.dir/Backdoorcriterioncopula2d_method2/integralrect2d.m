function [vxy] = integralrect2d(x,y,fxy)
%This function computes the integral of a 3d function by creating
%3d parallelepipedois. It assumes that all possible permutations of x and y
%are present, x = Xg(:),y = Yg(:) with [Xg,Yg] = meshgrid(xuniq,yuniq)
%Inputs
%       x: the x points
%       y: the y points
%       fxy: the corresponding function
%Output
%       vxy: The value of the integral


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We have the following result                      %
%   Ixy = dx*dy*Vxy                                 %
%                                                   %
%   with Vxy = sum_[i=2,i=N-1][j=2,N-1](f(xi,yj)) + %
%   1/2*sum_[i=2,i=N-1](f(xi,yN)+f(xi,y1)) +        %
%   1/2*sum_[j=2,j=N-1](f(x1,yj)+f(xN,yj)) +        %
%   1/4(f(x1,y1)+f(xN,y1)+f(x1,yN)+f(xN,yN))        %
%                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = size(fxy,1);
N = sqrt(n);

F = reshape(fxy,N,N);

sx = size(x,1);
sy = size(y,1);
sf = size(fxy,1);

if sx ~= sy || sx ~= sf
    error('All input should be of same size')
end

xu = sort(unique(x));
yu = sort(unique(y));

if length(xu) ~= length(yu)
    error('There must be the same number of x and y')
end

if length(xu) ~= sqrt(sx)
    error('Dimensions incorrect for having all permutations in the given dataset\n The number of unique values for x is %d while the size of fxy is %d',length(xu),sx)
end

sx = length(xu);
dxs = diff(xu);
dys = diff(yu);

if abs(sum(diff(dxs)))/dxs(1) > 5e-2 || abs(sum(diff(dys)))/dys(1) > 5e-2
    error('Points are not equally spaced');
end

dx = xu(2) - xu(1);
dy = yu(2) - yu(1);
base = dx*dy;

%(f(x1,y1)+f(xN,y1)+f(x1,yN)+f(xN,yN))
corners= (F(1,1)+F(N,1)+F(1,N)+F(N,N));
%sum_[i=2,i=N-1][j=2,N-1](f(xi,yj))
sij = sum(sum(F(2:N-1,2:N-1)));
%sum_[i=2,i=N-1](f(xi,yN)+f(xi,y1))
si = sum(F(:,N)+F(:,1));
%1/2*sum_[j=2,j=N-1](f(x1,yj)+f(xN,yj))
sj = sum(F(1,:)+F(N,:));

vxy = base*(sij+1/2*si+1/2*sj+1/4*corners);