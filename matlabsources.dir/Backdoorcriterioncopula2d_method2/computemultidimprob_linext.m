function [fx,x] = computemultidimprob_linext(ds,dimx,N,methodk,methodc)
%This function uses kernel to compute marginals and copula for computing
%the multidimensional probability 
%input
%           ds: dataset
%           dimx: the dimesions for the multivariate
%           N: The number of points for density support
%           methodk: kernel type [default: normal]
%           methodc: copula type [default: t]
%output
%       [x,fx]

n = size(ds,1);
p = size(ds,2);
nx = size(dimx,2);

if nargin < 3
    error('Not enough args, see help');
elseif nargin == 3
    methodk = 'normal';
    methodc= 't';
elseif nargin == 4
    methodc = 't';
elseif nargin > 5
    error('Too many args (%d), see help',nargin);
end

fprintf('Starting the computation of the multivariate pdf for %d pars\n',nx)

%compute marginals
margins_1c = zeros(nx,n);
margins_1p = zeros(nx,n);
support_1 = zeros(nx,n);
margins_2c = zeros(nx,N);
margins_2p = zeros(nx,N);
support_2 = zeros(nx,N);

for i = 1:nx
    d = dimx(i);
    support_2(i,:) = linspace(min(ds(:,d)),max(ds(:,d)),N);
    [margins_1c(i,:),support_1(i,:)] = ksdensity(ds(:,d),ds(:,d),'kernel',methodk,'function','cdf');
    margins_1p(i,:) = ksdensity(ds(:,d),ds(:,d),'kernel',methodk,'function','pdf');
    margins_2c(i,:) = ksdensity(ds(:,d),support_2(i,:)','kernel',methodk,'function','cdf');
    margins_2p(i,:) = ksdensity(ds(:,d),support_2(i,:)','kernel',methodk,'function','pdf');
end

%compute multidim
if strcmp(methodc,'t')
    [rho,nu] = copulafit('t',margins_1c');
    if nx == 2
        [Xg,Yg] = meshgrid(support_2(1,:),support_2(2,:));
        [Xcg,Ycg] = meshgrid(margins_2c(1,:),margins_2c(2,:));
        [Fxg,Fyg] = meshgrid(margins_2p(1,:),margins_2p(2,:));
        cx = copulapdf('t',[Xcg(:),Ycg(:)],rho,nu);
        fx = cx.*Fxg(:).*Fyg(:);
        x = [Xg(:),Yg(:)];
    else
        cx = copulapdf('t',margins_2c',rho,nu);
        fx = cx;
        for i = 1:nx
            fx=fx.*margins_2p(i,:)';
        end
        x = support_2;
    end
elseif strcmp(methodc,'Gaussian')
    [rho] = copulafit('Gaussian',margins_1c');
    if nx == 2
        [Xg,Yg] = meshgrid(support_2(1,:),support_2(2,:));
        [Xcg,Ycg] = meshgrid(margins_2c(1,:),margins_2c(2,:));
        [Fxg,Fyg] = meshgrid(margins_2p(1,:),margins_2p(2,:));
        cx = copulapdf('t',[Xcg(:),Ycg(:)],rho);
        fx = cx.*Fxg(:).*Fyg(:);
        x = [Xg(:),Yg(:)];
    else
        cx = copulapdf('Gaussian',margins_2c',rho);
        fx = cx;
        for i = 1:nx
            fx=fx.*margins_2p(i,:)';
        end
        x = support_2;
    end
else
    error('Only t and Gaussian copulae supported');
end