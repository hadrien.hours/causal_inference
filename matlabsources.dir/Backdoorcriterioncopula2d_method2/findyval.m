function [fzxl,zxl] = findyval(vyl,vyu,fyzxl,yzxl)
%This function is subsampling on the first dimension of 4 dimensional function and average for the matching samples
%It assumes that the data is organized by ndgrid and average (equally spaced samples) for the three remaining dimensions matching the values in the interval defined for the first dimension
%inputs
%	vyl: lower value of interval
%	vyu: upper value of interval
%	yzxl: support of 4 dim function
%	fyzl: 4 dim function

if nargin ~= 4
	error('Not enough samples, see help')
end

n = size(fyzxl,1);%supposed to be N^4
%sanity check
ns = n^(1/4);
r = floor(ns)-ns;

if r ~= 0
	error('The samples are not ndgrid organized, dimension mismatch')
end

%select the samples matching the interval of the first dimension
Iy = find(yzxl(:,1)<= vyu & yzxl(:,1)>=vyl);
fprintf('The number of matching values for y in [%.2g , %.2g] is %d\n',vyl,vyu,size(Iy,1))
sfzx = size(Iy,1);
zxls = yzxl(Iy,2:end);
fzxls = fyzxl(Iy);
%Count how many samples, in the first dim space, are matching the interval
vys = unique(yzxl(:,1));
Iyu = find(vys<=vyu & vys>=vyl);
sfy = size(Iyu,1)
if sfy == 1
	fxl = fzxls;
	zxl = zxls;
	return
else
	d1 = zxls(sfy+1,1)-zxls(1,1);%The difference between two consecutive value of z1
	d2 = zxls(sfy*ns+1,2)-zxls(1,2);%The difference between two consecutive value of z2
	d3 = zxls(sfy*ns^2+1,3)-zxls(1,3);%The difference between two consecutive value of x
	delta = d1*d2*d3;
end
%The size of subfunction has to be originalsize/numberOfSamplesInIntervalDim1
sf = sfzx/sfy;
if (floor(sf)-sf) ~= 0
	error('Unmatching size when subsampling');
end
%Build the subfunction
fzxl = zeros(sf,1);
zxl = zeros(sf,3);
for i = 1:sf
	idxl = (i-1)*sfy+1;
	idxu = i*sfy;
	fzxl(i) = mean(fzxls(idxl:idxu));
	mz = mean(zxls(idxl:idxu,:));
	%zxls(idxl,:)
	sz = sum(mz-zxls(idxl,:));
	if  sz > delta*eps%delta*eps and not 0 because of matlab rounding approximations
		sz
		delta*eps
		error('The samples are not ndgrid organized, samples mismatch')
	end
	zxl(i,:) = zxls(idxl,:);
end
