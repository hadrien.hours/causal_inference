function [x,fx,ex,sx,mx,ex2,sx2] = main_bdcopula0d_wrapping(pathds,dimx,dimy,valy,deltay,N,methodk,methodc,pathout)
%This function estimated the probability density function of X after
%intervention on Y given the back door variable Z
%This function is restricted to the case where dim(Y) = 1 and dim(Z) = 1
%Inputs
%       ds: the dataset
%       dimx: The X dimension
%       dimy: The Y dimension
%       valy: The value of Y intervention
%       deltay: The interval width around valy for estimation
%       dimz: The Z dimension
%       N: The number of points for the support of marginals and pdfs
%       methodk: The kernel type [default normal]
%       methodc: The copula type [default t]
%Outputs
%       x: value of x
%       fx: probabilyt of X=x after intervention
%       ex: expected value post intervention
%       sx: standard deviation of x post intervention
%       mx: mean value of x in the original dataset for the range of
%       corresponding y
%       ex2 : expected value of x in the original dataset for the range of
%       corresponding y
%       sx2: standard deviation of x in the original datasset for the range
%       of corresponding y
%

ds = csvread(pathds,1,0);

n = size(ds,1);
p = size(ds,2);

if nargin < 9
    error('Not enough args, see help');
elseif nargin > 9
    error('Too many args, see help');
end


vyu = valy+deltay/2;
vyl = valy-deltay/2;

Iy = find(ds(:,dimy) <= vyu & ds(:,dimy) >= vyl);

fprintf('%d samples found for the given interval [%.2g,%.2g]\n',size(Iy,1),vyl,vyu);


if size(Iy,1) == 0
    error('No value found for the given interval around Y')
end

X = ds(Iy,dimx);
Y = ds(Iy,dimY);

%Compute pre intervention metrics
mx = mean(X);
xs = linspace(min(X),max(X),N);
PDFx = ksdensity(X,xs,'kernel',methodk,'function','pdf');
hx = @(t) interp1(xs,PDFx.*xs,t,'cubic');
ex2 = integral(hx,min(xs),max(xs));
hx2 = @(t) interp1(xs,xs.^2.*PDFx,t,'cubic');
m2 = integral(hx2,min(xs),max(xs));
sx2 = sqrt(m2-ex2^2);


fprintf('Start computing the conditional pdf of X given Y \n');
[fxcy,xyl] = computecondprob1d([X,Y],1,2,N,methodk,methodc);

I = find(xyl(:,2)<=vyu & xyl(:,2) >= vyl);

fx = fxcy(I);
x = xyl(I,1);

h = @(t) interp1(x,x.*fx,t,'cubic');
ex = integral(h,min(x),max(x));
h2 = @(t) interp1(x,x.^2.*fx,t,'cubic');
m2 = integral(h2,min(x),max(x));
sx = sqrt(m2 - ex^2);

if size(I,1) == 0
    error('No matching value for Y in the computation of fx.y for y = %.2g +/- %.2g\n',valy,deltay/2)
end

%Write results
probaf=strcat(pathout,'probability_y_',num2str(dimx),'_post_intervention_on_x_',num2str(dimy),'_',num2str(valy),'_',num2str(deltay),'_kernel_',methodk,'_copula_',methodc,'_',num2str(N),'_npoints.csv');
csvwrite(probaf,[xyl(I,:),fxcy]);
statf=strcat('statistics_y_',num2str(dimx),'_post_intervention_on_x_',num2str(dimy),'_',num2str(valy),'_',num2str(deltay),'_kernel_',methodk,'_copula_',methodc,'_',num2str(N),'_npoints.csv');
fid = fopen(statf,'wt');
fprintf(fid,'%s,%s,%s,%s,%s\n','ey_preinter','sy_preinter','my_preinter','ey_postinter','sy_postinter');
fprintf(fid,'%.2g,%.2g,%.2g,%.2g,%.2g\n',ex2,sx2,mx,ex,sx);
fclose(fid);