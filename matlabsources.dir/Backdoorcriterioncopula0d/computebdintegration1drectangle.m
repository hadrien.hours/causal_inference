function [fx,x] = computebdintegration1drectangle(fxczl,fzl,xzl,zl)
%This function is to be used with main_bdcopula2d. It computes the
%probability of f_X/do(Y=y) with the formula of the backdoor criteria
%integrating f_x/z,yf_z
%Inputs
%       fxczl = the density probability of fx/z,y
%       fzl = the density probability of fz
%       xzl = the values of x and corresponding to fxzcl
%       zl = the values of z corresponding to fzl
%Outputs
%       fx = the probability of X after intervening on Y
%       x = the X values

xu = unique(xzl(:,1));
sx = size(xu,1);
fx = zeros(sx,1);

fprintf('The number of unique values of X for whicht f_X/do(Y=y) will be computed is %d\n',sx);

verbose =1;

if verbose > 0
   fprintf('The dims of the conditional pdf of X/Z is %d,%d\n',size(fxczl))
   fprintf('The dims of the support of the conditional pdf X/Z is %d,%d\n',size(xzl))
   fprintf('The dims of the marginal of Z is %d,%d\n',size(fzl));
   fprintf('The dims of the Z pdf support is %d,%d\n',size(zl));
end

for i = 1:sx
    if mod(i,floor(sx/10))== 1
        fprintf('Starting computation of  value %d on a total of %d \n',i,sx);
    end
    Ix = find(xzl(:,1)==xu(i));
%         fprintf('The number of conditional pdf values for which %.2g is defined is %d\n',xu(i),size(Ix,1))
    zv = xzl(Ix,2);%list of z for which the value of f_x/z is defined
%         fprintf('Size zv is %d,%d\n',size(zv,1),size(zv,2))
    %look for the corresponding values of z and subset the corresponding fz
    fzv = zeros(size(zv,1),1);
    for j = 1:size(zv,1)
%         I = find(zl(:,1) ==zv(j,1));
%         fprintf('For the value of pdf conditional support X = %.2g, Z = %.2g, we found %d values for the marginal\n',xu(i),zv(j,1),size(I,1))   
        fzv(j) = fzl(zl(:,1)==zv(j,1));
    end
    %Create the function of z f_x/z*f_z
    %F = TriScatteredInterp(zv(:,1),zv(:,2),fzv.*fxczl(Ix),'nearest');
    %h = @(z1,z2) F(z1,z2);
    %Integrate over z
        %fprintf('Start integrating on both dimension of Z for X value %.2g\n',xu(i))
    %fx(i) = integral2(h,min(zv(:,1)),max(zv(:,1)),min(zv(:,2)),max(zv(:,2)));
    fbd = fzv.*fxczl(Ix);
    h = @(t) interp1(zv,fbd,t,'nearest');
    fx(i) = integral(h,min(zv),max(zv));
end
x = xu;
