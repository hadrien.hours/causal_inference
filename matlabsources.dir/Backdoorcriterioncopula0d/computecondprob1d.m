function [Pxz,supportpdfxz_x] = computecondprob2d(ds,dimx,dimz,N,methodk,methodc)
%This function estimated the condtional probability density function of X 
%conditionning on Z
%This function is restricted to the case where dim(Z) = 1
%Inputs
%       ds: the dataset
%       dimx: The X dimension
%       dimz: The Z dimensions
%       N: The number of points for the support of marginals and pdfs
%       methodk: The kernel type [default normal]
%       methodc: The copula type [default t]
%Outputs
%       xzl: value of x and z
%       fx: probabilyt of X=x conditioned on Z=z

n = size(ds,1);
p = size(ds,2);

if nargin < 4
    error('Not enough args, see help');
elseif nargin == 4
    methodk = 'normal';
    methodc = 't';
elseif nargin == 5
    methodc = 't';
elseif nargin > 6
    error('Too many args, see help');
end

if size(dimz,2) ~= 1
    error('conditionning set must be of size 1');
end

X = ds(:,dimx);
Z = ds(:,dimz);

%compute marginals
margincx_1 = zeros(1,n);
supportx_1 = X';
margincx_2= zeros(1,N);
%use linearly spaced point support
supportx_2 = linspace(min(X),max(X),N);
margincz_1 = zeros(1,n);
supportz_1 = Z';
margincz_2 = zeros(1,N);
supportz_2 = zeros(1,N);
%use linearly spaced point support
supportz_2(1,:) = linspace(min(Z(:,1)),max(Z(:,1)),N);
    %compute CDF marginals for copula parameters estimations
    fprintf('Starting computing marginal for CDF of X\n');
margincx_1 = ksdensity(supportx_1,supportx_1,'kernel',methodk,'function','cdf');
    %remove extremes for copulafit
margincx_1(margincx_1==0) = eps;
margincx_1(margincx_1==1) = 1-eps;
    fprintf('Starting computing marginals for CDFs of Z\n');
Fz1(1,:) = ksdensity(supportz_1(1,:),supportz_1(1,:),'kernel',methodk,'function','cdf');
    %remove extremes for copulafit
Fz1(Fz1==0) = eps;
Fz1(Fz1==1) = 1-eps;
    %compute CDF marginals for density support
    fprintf('Compute marginal for CDF of X for linearly spaced point for the support\n');
margincx_2= ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','cdf');
%remove extremes for copulafit
margincx_2(margincx_2==0) = eps;
margincx_2(margincx_2==1) = 1-eps;
    fprintf('Compute marginals for CDFs of Z for linearly spaced point for the support\n');
Fz2(1,:) = ksdensity(supportz_1(1,:),supportz_2(1,:),'kernel',methodk,'function','cdf');%use linearly spaced point support
%remove extremes for copulafit
Fz2(Fz2==0) = eps;
Fz2(Fz2==1) = 1-eps;

%Compute bivariate PDF of X conditionally on Z
    %compute x pdf
marginpx_2 = ksdensity(supportx_1,supportx_2,'kernel',methodk,'function','pdf');
fprintf('Finished computing the marginal pdf of X for conditional PDF computing of f_x/z (npoints = %d)\n',size(marginpx_2,1));
    %Estimate copula
if strcmp(methodc,'t')
    %estimate parameters with CDFs of samples points
    fprintf('Size of CDFx is %d,%d CDFz is %d,%d\n',size(margincx_1),size(Fz1));
    [rhoxz,muxz] = copulafit('t',[margincx_1',Fz1']);
    fprintf('Parameters computed for the bivariate copula of c_x,z (mu = %.2g, rhoxz of size %d,%d)\n',muxz,size(rhoxz,1),size(rhoxz,2));
    %compute bivariate pdfs at the meshgrid points
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    [Xcdfx,Xcdfz] = meshgrid(supportx_2,supportz_2);
    supportpdfxz_x = [Xcdfx(:),Xcdfz(:)];
    [Ycdfx,Ycdfz] = meshgrid(margincx_2',Fz2);
    supportcdfx = Ycdfx(:);
    supportcdfz = Ycdfz(:);
    [PDFxg,Tmp] = meshgrid(marginpx_2,marginpx_2);
    supportpdfx = PDFxg(:);
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^2);
    Cxz = copulapdf('t',[supportcdfx,supportcdfz],rhoxz,muxz);
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
elseif strcmp(method,'Gaussian')
    %estimate parameters with CDFs of samples points
    rhoxz = copulafit('t',[margincx_1',Fz1]);
    fprintf('Parameters computed for the bivariate copula of c_x,z (rhoxz of size %d,%d)\n',size(rhoxz,1),size(rhoxz,2));
    %compute bivariate pdfs at the meshgrid points
    fprintf('Starting creating the supports for grid linearly spaced point for the bivariate PDF f_x,z\n');
    [Xcdfx,Xcdfz] = meshgrid(supportx_2,supportz_2);
    supportpdfxz_x = [Xcdfx(:),Xcdfz(:)];
    [Ycdfx,Ycdfz] = meshgrid(margincx_2',Fz2);
    supportcdfx = Ycdfx(:);
    supportcdfz = Ycdfz(:);
    [PDFxg,Tmp] = meshgrid(marginpx_2,marginpx_2);
    supportpdfx = PDFxg(:);
    fprintf('Starting computing the copula points for c_x,z for %d points\n',N^2);
    Cxz = copulapdf('Gaussian',[supportcdfx,supportcdfz],rhoxz);
    Pxz = Cxz.*supportpdfx;
    fprintf('Finished estimating the conditional PDF of f_x/z\n')
end
