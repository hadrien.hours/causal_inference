function [modelpath] = learn_struct_wrapper(algo,pathds,pathout,indepc,pathres,threshold)
%This function is a generic function to run pc/ic algorithm with different
%possible independence criteria and write the output in a csvfile
%Inputs
%       algo = string defining algorithm (PC or IC)
%       pathds = path to csvfile containing the data (assumed with header)
%       pathout = output dir where the result will be stored
%       indepc = string defining the type of independence criterion to use
%       (ZFisher, HSIC, Oracle)
%       pathres = path to oracle file results, empty if HSIC or ZFisher
%       used (the file is assumed to be in the format
%       [X,Y,Z1,Z2,Z3,...,alpha,N,l,percentage])
%       threshold: float being the significance level to use, in case
%       ZFisher or HSIC is used, or the percentage threshold to use for
%       testing oracle (0.5 is the advised value)
%Output
%       pathtocsvfile containing the matrix corresponding to inferred graph

ds = csvread(pathds,1,0);
n = size(ds,1);
p = size(ds,2);
namealgo = '';
nameindep = '';

if strcmp(algo,'PC')
    namealgo = 'pc';
    if strcmp(indepc,'ZFisher')
        nameindep = 'zfisher';
        res = learn_struct_pdag_pc('cond_indep_fisher_z',p,p,corr(ds),n,threhsold);
    elseif strcmp(indepc,'HSIC')
        nameindep = 'hsic';
        res = learn_struct_pdag_pc('indtestimpl',p,p,ds,threshold);
    elseif strcmp(indepc,'Oracle')
        nameindep = 'oracle';
        res = learn_struct_pdag_pc('testindepfromdb_pars',p,p,pathres,threshold);
    end
elseif strcmp(algo,'IC')
    namealgo = 'ic';
    if strcmp(indepc,'ZFisher')
        nameindep = 'zfisher';
        res = learn_struct_pdag_ic_star('cond_indep_fisher_z',p,p,corr(ds),n,threhsold);
    elseif strcmp(indepc,'HSIC')
        nameindep = 'hsic';
        res = learn_struct_pdag_ic_star('indtestimpl',p,p,ds,threshold);
    elseif strcmp(indepc,'Oracle')
        nameindep = 'oracle';
        res = learn_struct_pdag_ic_star('testindepfromdb_pars',p,p,pathres,threshold);
    end
end

fileout = strcat(pathout,'/',namealgo,'_',nameindep,'_',num2str(threshold),'.csv');

csvwrite(fileout,res);

fprintf('The #causal model# was saved in:%s\n',fileout)

modelpath = fileout;
