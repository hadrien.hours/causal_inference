function [listEdges] = findEges(M)
%This function returns the list of edges from a given graph
%Input
%       Square matrix containing -1,1,-2,2 for where there is an edge between
%       i and j
%Output
%       n*3 matrix: parent,child,type of edge
%                   type is 1,2,3,4 depending on the type of edge
%                   (undirected,latent directed, marked)

n = size(M,1);
p = size(M,2);

if n ~= p
    error('The matrix has to be square')
end

listEdges = [];

%Bidirected edges
[r,c] = find(M==1);
if size(r,1) ~= 0
    %if [r,c] = 1 then [c,r] = 1
    if mod(size(r,1),2) ~= 0
        error('Incorrect formatting of the Matrix (bidirected edge should be i,j and j,i');
    end
   sr = size(r,1);
   s = 1;
   while s <= sr
       listEdges = [listEdges;r(s) c(s) 1];
       idx = find(r==c(s) & c == r(s));
       if isempty(idx)
           error('Incorrect formatting of the Matrix (bidirected edge should be i,j and j,i');
       else
           r(idx) = [];
           c(idx) = [];
           sr = sr -1;
       end
       s = s+1;
   end
end

%Bidirected edges
[r,c] = find(M==2);
if size(r,1) ~= 0
    %if [r,c] = 1 then [c,r] = 1
    if mod(size(r,1),2) ~= 0
        error('Incorrect formatting of the Matrix (bidirected latent edge should be i,j and j,i');
    end
   sr = size(r,1);
   s = 1;
   while s <= sr
       listEdges = [listEdges;r(s) c(s) 2];
       idx = find(r==c(s) & c == r(s));
       if isempty(idx)
           error('Incorrect formatting of the Matrix (bidirected latent edge should be i,j and j,i');
       else
           r(idx) = [];
           c(idx) = [];
           sr = sr -1;
       end
       s = s+1;
   end
end

%Directed edges
[r,c] = find(M==-1);
if size(r,1) ~= 0
   sr = size(r,1);
   for s = 1:sr
       listEdges = [listEdges;r(s) c(s) 3];
   end
end

%Directed marked edges
[r,c] = find(M==-2);
if size(r,1) ~= 0
   sr = size(r,1);
   for s = 1:sr
       listEdges = [listEdges;r(s) c(s) 4];
   end
end