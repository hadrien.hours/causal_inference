function [D] = estimate_direct_effect_ln(pathlisteffect,pathds,fout)
%This function make a condtional linear regression after converting pars 
%into log linear models. The coefficient of X is the direct effect
%This function takes as input a csvfile where first column is x second is y
% and from the third the blocking set Z in the single door criterion from
% Pearl
%Input
%   pathlisteffect: csvfile with header in the form [x,y,z1,...] z1 = 0 if
%   empty set
%   pathds: csvfile with the dataset, with header
%   fout: the file where the results will be written


listeffect  = csvread(pathlisteffect,1,0);
ds = csvread(pathds,1,0);

ne = size(listeffect,1);

D = zeros(ne,3);

for i = 1:ne
    x = listeffect(i,1);
    y = listeffect(i,2);
    z = listeffect(i,3:end);
    X = ds(:,x);
    Y = ds(:,y);
    if z == 0
        %regression Y on X carreful 0 value when passing to log
        Y(Y==0)=eps;
        X(X==0)=eps;
        Yl = log(Y);
        Xl = log(X);
        P = polyfit(Xl,Yl,1);
        D(i,1) = x;
        D(i,2) = y;
        D(i,3) = P(1);
    else
       Z = ds(:,z);
       for j = 1:size(Z,2)
           Z(Z(:,j)==0,j)=eps;
       end
       O = ones(size(X,1),1);
       Regressors = [X,Z,O];
       P = regress(Y,Regressors);
       D(i,1) = x;
       D(i,2) = y;
       D(i,3) = P(1);
       % regression Y on X and Z and take coeff on X, careful with 0 values
       % in X,Y or Z for log
    end
end

fid = fopen(fout,'wb');
header = 'x,y,coefficient';
fprintf(fid,'%s\n',header);
dlmwrite(fout,D,'-append','delimiter',',','newline','unix');