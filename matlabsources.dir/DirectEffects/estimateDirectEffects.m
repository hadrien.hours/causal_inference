function [res,pathresults] = estimateDirectEffects(pathds,pathout,percentage_extr,pathmodel,method)
%This function estimate the direct effect from any parameter being the
%parent of another
%Inputs
%       pathds: path to csvfile containing the dataset (with header)
%       pathout: path to dir where the results will be stored
%       percentage_extr: percentage to remove outliers [optional]
%       pathmodel: path to csvfile containing the matrix corresponding to
%       the causal graph
%       method: string corresponding to the method to use for regression
%Output
%       res: result in the form [x,y,blockingset,value]
%       pathresults: where the results where stored

if nargin < 4
    error('Not enough args, see help');
elseif nargin == 4
    perc = 0;
    rem_ext = 0;
elseif nargin == 5
    perc = percentage_extr;
    rem_ext = 1;
else
    error('Too many inputs, see help')
end

ds= csvread(pathds,1,0);

if rem_ext == 1
    dsc = remove_extremes(ds,perc);
else
    dsc = ds;
end

model = csvread(pathmodel);

%Get edges and types
listEdges = findEdges(model);

if size(listEdges,1) == 0
    error('The given model contains no edge (or none of the recognized type (1,2,-1,-2)')
end

%Select only directed edges
listDirectedEdges = listEdges(listEdges(:,3) == 3 || listEdges == 4);

sl = size(listDirectedEdges,1);

res = cell(sl,4);
maxbsize = 0;
if sl == 0
    error('The given model contains no directed edge (or none of the recognized type (-1,-2)')
end

for e = 1:sl
    par = listDirectedEdges(e,1);
    chi = listDirectedEdges(e,2);
    res{e,1} = par;
    res{e,2} = chi;
    z = findBlockingSet(par,chi,model);%TODO
    if size(z,2) > maxbsize
        maxbsize = size(z,2);
    end
    res{e,3} = z;
    val = ConditionalRegressionWrapper(ds,par,chi,z,method);%TODO
    res{e,4} = val;
end

%Write in a csvfile
fileout = strcat(pathout,'/direct_effect_',method,'_remove',num2str(perc),'_extremum.csv');
fid = fopen(fileout,'wb');
header = 'x,y';
for s = 1:maxbsize
    header = strcat(header,',z',num2str(s));
end
header = strcat(header,',result');
fprintf(header,fid);
for e = 1:sl
    s= num2str(res{e,1});
    s = strcat(s,',',num2str(res{e,2}));
    z = res{e,3};
    for i = 1:size(z,2)
        s = strcat(s,',',num2str(z(i)));
    end
    %padding with zeros so that all the lines have the same size
    if size(z,2) < maxbsize
        pl = maxbsize-size(z,2);
        for i = 1:pl
            s = strcat(s,',');
        end
    end
    s = strcat(s,',',num2str(res{e,4}));
    fprintf(s,fid);
end