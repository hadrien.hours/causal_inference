function [dsets] = findDsepSet(x,y,M)
%This function returns the d-separating sets separting X and Y in the graph
%G
%Inputs
%       x: dim of X
%       y: dim of Y
%       M: Matrix representing the graph (with 1,2,-1,-2) see PC/IC algo
%Output
%       dsets: set of d-separating sets

[listp] = findTrecks(x,y,M);

%DEPTH FIRST SEARCH / BREADTH FIRST SEARCH
% SELECT ANY PATH CONTAINING x AND y --> y<-r->x has to be found... WHERE
% TO START ? x ? Then go through edge ends...