function [Zc] = findBlockingSet(par,chi,model)
%This function returns the blocking set to estimate the direct effect from
%a parent to a child using the single door criterion
%Input
%       par: dimension of parent variable
%       chi: dimension of child variable
%       model: n*n matrix representing the causal model and containing
%       values from {1,2,-1,-2}
%Output
%       z: blocking set

Mr = model;
%G_alpha, remove x -> y
Mr[par,chi] = 0;

%Find all the d-separating set between x and y in G_alpha
Z = findDsepSet(par,chi,Mr);%TODO

%Remove the containing descendants of Y
Zc = [];
for i = 1:size(Z,1)
    flag = 0;
    for j = 1:size(Z(i,:),2) 
       [path] = findPathFrom(chi,Z(i,j),model);%TODO
       if size(path,1) ~= 0
           flag = 1;
           break;
       end
    end
    if flag == 0
        Zc = [Zc;Z];
    end
end