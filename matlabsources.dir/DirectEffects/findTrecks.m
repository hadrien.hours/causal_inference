function [paths] = findTrecks(X,Y,M)
%this functions returns all the trecks between X and Y in the graph M
% Inputs
%           x: dim of X
%           y: dim of Y
%           M: n*n matrix describing the graph with 1,2,-1,-2
% Outputs
%           paths: list of treck between X and Y