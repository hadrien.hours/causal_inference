function [yv,fy,ey,sy,my,ey2,sy2] = main_fdcopula1d(ds,y,x,z,valx,deltax,N,methodk,methodc)
%This function estimates the prediction of an intervention on X, on Y using
%the front door criterion in the case where dim(Z) = 1
% P(Y=y | do(X=x)) = sum_{z}[P(Z=z | X = x)*sum_{x'}P(Y=y | X=x',Z=z)
%
%Inputs
%   ds : the dataset
%   y: the y dim
%   x: the x dim
%   z: the z dim
%   valx: the x value of intervention
%   deltax: the interval around the given value for estimation of
%   intervention
%   N: support size for marginals estimations
%   methodk[optional]: kernel method [default, normal]
%   methodc[optional]: Copula method [default,t]
%
%Outputs
%   yv: y values support of the post intervention pdf
%   fy: pdf post intervention
%   ey: mean value of y post intervention
%   sy: std value of y post intervention
%   my: mean value of y average value pre intervention for X=x
%   ey2: mean value of y pre intervention by conditionning and estimating
%   the pdf
%   sy2: std value pre intervention

n = size(ds,1);
p = size(ds,2);

if nargin < 7
    error('Not enought inputs, see help')
elseif nargin == 7
    methodk = 'normal';
    methodc = 't';
elseif nargin == 8
    methodc = 't';
elseif nargin > 9
    error('Too many inputs, see help')
end

if y > p || x > p || z > p
    error('Wrong parameters values (out of size (%d))',p)
end

if length(z) > 1
    error('This function assumes a front door criterion variable of dim 1')
end

vxl = valx - deltax/2;
vxu = valx + deltax/2;

Ix = find(ds(:,x) <= vxu && ds(:,x) >= vxl);
sIx = size(Ix,1);
if sIx < 5
    error('Not enough samples found for estimating the prediction (%d)',sIx)
end

X = ds(:,x);
Y = ds(:,y);
Z = ds(:,z);

%Estimation of marginals
    %Estimated on the same measurement point
    CDFx_1 = ksdensity(X',X','kernel',methodk,'function','cdf');
    CDFy_1 = ksdensity(Y',Y','kernel',methodk,'function','cdf');
    CDFz_1 = ksdensity(Z',Z','kernel',methodk,'function','cdf');
    %Estimation on the linearly spaced points
    support_x2 = linspace(min(X),max(X),N);
    support_y2 = linspace(min(Y),max(Y),N);
    support_z2 = linspace(min(Z),max(Z),N);
    CDFx_2 = ksdensity(X',supportx_2,'kernel',methodk,'function','cdf');
    CDFy_2 = ksdensity(Y',supporty_2,'kernel',methodk,'function','cdf');
    CDFz_2 = ksdensity(Z',supportz_2,'kernel',methodk,'function','cdf');
    
%Estimate the bivariate CDF of F_{X,Z}
[CDFxg_1,CDFzg_1] = meshgrid(CDFx_1',CDFz_1');
[Xg1,Zg1] = meshgrid(X',Z');
[CDFxg_2,CDFzg_2] = meshgrid(CDFx_2',CDFz_2');
[Xg2,Zg2] = meshgrid(support_x2',support_x2');
if strcmp(methoc,'t')
    [rhoxz,muxz] = copulafit('t',[CDFxg_1(:),CDFzg_1(:)]);
    Fxz2 = copulacdf('t',[CDFxg_2(:),CDFzg_2(:)],rhoxz,muxz);
%     Fxz2r = reshape(Fxz2,N,N);
%     Xg2r = reshape(Xg2,N,N);
%     Zg2r = reshape(Zg2,N,N);
elseif strcmp(methodc,'Gaussian')
    [rhoxz] = copulafit('Gaussian',[CDFxg_1(:),CDFzg_1(:)]);
    Fxz2 = copulacdf('Gaussian',[CDFxg_2(:),CDFzg_2(:)],rhoxz);
%     Fxz2r = reshape(Fxz2,N,N);
%     Xg2r = reshape(Xg2,N,N);
%     Zg2r = reshape(Zg2,N,N);
end

%Estimate conditional pdf of Y/X,Z
