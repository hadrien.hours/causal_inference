function [p] = hsic_bootstrap_wrapper(pathds,i,j,k,alpha,N,l,pathout)
%This function is a wrapper for hsicimpl_nloop
%Input
%       pathds: csvfile with header
%       i:  dim of X
%       j:  dim of Y
%       k:  dim(s) of Z
%       alpha: significance in subtest of HSIC
%       N:  subdataset size
%       l:  number of loops in the bootstrap
%       pathout:    csvfile to store result
%Output
%       p: percentage of positive outcome in testing X indep Y cond Z

if nargin ~= 8
    error('Wrong number of args, see help');
end

ds = csvread(pathds,1,0);

n = size(ds,1);
p = size(ds,2);

if i > p 
    error('Parameter X out of bound (%d parameters and %d dim for X',p,i);
end

if j > p 
    error('Parameter Y out of bound (%d parameters and %d dim for Y',p,j);
end

flag_z = 0;
zv = 0;

for zd = 1:size(k,2)
    if (k(zd) > p)
        zv = k(zd);
        flag_z = 1;
        break
    end
end

if flag_z > 0
    error('Z dimension %d out of bound (%d parameters)',zv,p)
end

[res,p] = indtestimpl_nloop_sval_notic(i,j,k,ds,alpha,S,N);

csvwrite(pathout,[i,j,k,alpha,N,l,p]);